USE [safb_dq]
GO

BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_OPPORTUNITY_CONTACT_ROLE

END 

BEGIN -- CREATE IMP 

					select distinct 
							 [OpportunityId] = xt.id					--LOOKUP: Lookup (Opportunity)
							,[ContactId] = xc.id						--LOOKUP: Lookup (Contact)
							,[Role] = 'Soft Credit' -- Role = "Soft Credit"

			 				--reference
							,t1.[Donation_Child_ID] as zrefDonId
							,t1.[Donation_Soft_Link] as zrefDonSoftLinkId
							,th.[stgRecordTypeName], th.[stgAccountType]
			 		into [safb_migration].DBO.IMP_OPPORTUNITY_CONTACT_ROLE
			
					from [safb_dq].[dbo].[tblDonation] as [t1]
					join [safb_dq].[dbo].[tblHeader] as [th] on [t1].[Donation_Soft_Link]=[th].[ID_Num]
					join [safb_migration].[dbo].[xtr_contact] as [xc] on [th].[Legacy_Id__c]=[xc].[Legacy_Id__c]
					join [safb_migration].dbo.xtr_opportunity as xt on  'DQ-'+Cast(t1.Donation_Child_ID	as Varchar(30))=xt.legacy_id__c
				    where th.stgRecordTypeName='Household'   ---OppContRole only to hh contacts. 
				    order by xc.id, xt.id
			 
			 
END -- TC1: 10190  tc2L 41691

BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_OPPORTUNITY_CONTACT_ROLE
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_OPPORTUNITY_CONTACT_ROLE group BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_XXXXXXXXXXXX
	 

END 