USE [safb_dq]
GO


 
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_ADDRESS

END 

BEGIN -- CREATE IMP 
					select	npsp__Household_Account__c = xa.id	 --LOOKUP: Lookup (Account)	
							,npsp__MailingStreet__c = t1.[Address]		
							,npsp__MailingStreet2__c = t1.Address2		
 							,npsp__MailingCity__c = t1.City		
							,npsp__MailingState__c = t1.[State]		
							,npsp__MailingPostalCode__c = t1.Zip		
							,County__c = t1.County		
							,npsp__MailingCountry__c = t1.stgCountry		
							,CreatedDate = cast(t1.When_Donor_Added	as date)
							,npsp__Address_Type__c = t1.npsp__Address_Type__c   		
							,Legacy_Id__c = t1.[Legacy_Id__c]
							,npsp__Default_Address__c ='TRUE'	-- Set to "TRUE" on all records				 
			 
							--reference
							, t1.ID_Num	 as zrefIDNum
							, null as zrefXAParentID
			 		into [safb_migration].DBO.IMP_ADDRESS
					from [safb_dq].dbo.[tblHeader] as t1
					inner join [safb_migration].dbo.xtr_account as xa on t1.legacy_id__c = xa.legacy_id__c
					where t1.stgRecordTypeName='Household'  --migrate address only on hh accts.
				union 
					select  npsp__Household_Account__c = xa.id	 --LOOKUP: Lookup (Account)
						    ,npsp__MailingStreet__c = t1.XA_Address	
							,npsp__MailingStreet2__c = t1.XA_Address2	
							,npsp__MailingCity__c = t1.XA_City	
							,npsp__MailingState__c = t1.XA_State	
							,npsp__MailingPostalCode__c = cast(t1.XA_Zip as nvarchar(20))
							,County__c = t1.XA_County	
							,npsp__MailingCountry__c = t1.stgXaCountry
							,CreatedDate = iif (t1.XA_When_Added is null, Cast(t1.[XA_When_Last_Edited] as Date), Cast(t1.XA_When_Added as Date))
							,npsp__Address_Type__c = xad.SF_FieldValue	
 							,Legacy_Id__c= 'DQ-XA-'+Cast(t1.[XA_Child_ID] as NVarchar(20))
							,npsp__Default_Address__c = 'FALSE'	-- Set to "FALSE"

							--reference
							,t1.[XA_Child_ID] as zrefIDNum
							,cast(t1.XA_Parent_ID as nvarchar(20)) as zrefXAParentID
					
					from [safb_dq].dbo.[tblExtraAddress] as t1
					inner join [safb_dq].dbo.[tblHeader] as [th] on t1.[XA_Parent_ID]=th.[ID_Num]
					inner join [safb_migration].dbo.xtr_account as xa on th.legacy_id__c = xa.legacy_id__c
					left join (select * from [safb_dq].dbo.[CHART_dq_address_type] as [cdat] 
								where [cdat].[field_name]='XA_Code') as xad on t1.[XA_Code]=xad.[code]
					where th.stgRecordTypeName='Household' --migrate address only on hh accts.
				


				 
END -- TC1: 80472; ;tc2:115,536

BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_ADDRESS
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_ADDRESS group BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

	--delete no valid address	 
		select * from [safb_migration].dbo.IMP_ADDRESS
		where npsp__MailingStreet__c is null and npsp__MailingStreet2__c is null
		and npsp__MailingCity__c is null and npsp__MailingState__c is null
		--11047

		delete [safb_migration].dbo.IMP_ADDRESS
		where npsp__MailingStreet__c is null and npsp__MailingStreet2__c is null
		and npsp__MailingCity__c is null and npsp__MailingState__c is null

  	--count 
		select COUNT(*) FROM [safb_migration].DBO.IMP_ADDRESS
		select count(*) from [safb_dq].dbo.[tblHeader] as [th]
		select count(*) from [safb_dq].dbo.[tblExtraAddress] as [tea]


		select * from [safb_dq].dbo.[tblHeader] as [th] where [th].[stgRecordTypeName] like '%org%'

		select * from [safb_dq].dbo.[tblExtraAddress] as [tea] 
end 

begin --exceptions

		select [errormessage], count(*) c 
		from [safb_migration].dbo.imp_Address_xerror1
		group by [errormessage]

		select * from [safb_migration].dbo.imp_Address_xerror1
		
		alter table [safb_migration].dbo.imp_Address_xerror1
		drop column ErrorCode, ErrorColumn, ErrorMessage

		truncate table [safb_migration].dbo.xtr_address
		select count(*) from [safb_migration].dbo.xtr_address
		select * from [safb_migration].dbo.xtr_address where legacy_id__c ='DQ-XA-10080'

		select ia.* 
		into [safb_migration].dbo.[IMP_ADDRESS_xcp] 
		from [safb_migration].dbo.[IMP_ADDRESS] as [ia]
		left join [safb_migration].dbo.xtr_address as xa on ia.[Legacy_Id__c]=xa.[Legacy_Id__c]
		where xa.[Legacy_Id__c] is null
		order by [ia].[npsp__Household_Account__c]




end
