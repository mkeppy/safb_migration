USE [safb_dq]
GO

--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
			0121I000000nGLJQA2	Account	Household Account
			0121I000000nGLKQA2	Account	Organization
	*/
 
 BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_RELATIONSHIP

END 

BEGIN -- CREATE IMP 
					select  [Legacy_Id__c] = [t1].[Legacy_Id__c] +'_'+t1.[Legacy_Id__2]
							,npe4__Contact__c = xc1.id
							,npe4__RelatedContact__c = xc2.id
							,npe4__Type__c = cdr.[npe4__Type__c]
							,npe4__Description__c = cdr.[npe4__Description__c]
							,CratedDate = t1.[When_Donor_Added] 
					
							--reference
							,zrefPrim_contact = t1.Legacy_Id__c  
							,zrefSec_contact = t1.[Legacy_Id__2]

							,zrefRecordTypeCnt1 = t1.[stgRecordTypeName]
							,zrefRecordTypeCnt2 = null
							,zrefIDNum = t1.[ID_Num]
							
			 		into [safb_migration].DBO.IMP_RELATIONSHIP
			
					from [safb_dq].dbo.[tblHeader] as t1
					inner join [safb_migration].dbo.xtr_contact as xc1 on t1.legacy_id__c = xc1.legacy_id__c
					inner join [safb_migration].dbo.xtr_contact as xc2 on t1.legacy_id__2 = xc2.legacy_id__c
					left join [safb_dq].dbo.[CHART_dq_relationship] as [cdr] on t1.[Relationship_Between_Primary_And_Secondary]=cdr.[Relationship_Between_Primary_And_Secondary]
					where t1.Legacy_Id__2 is not null and t1.[Legacy_Id__c] is not null

END -- TC1: 20,590; tc2;23246

BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_RELATIONSHIP
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_RELATIONSHIP group BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_CONTACT
	select count(*) from [safb_dq].dbo.[tblLinkage] as [tl]

	select * from [safb_migration].DBO.IMP_RELATIONSHIP
	where npe4__contACT__c is null or npe4__relatedcontact__c is null
	 

END 
