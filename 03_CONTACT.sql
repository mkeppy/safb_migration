USE [safb_dq]
GO

--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
			0121I000000nGLJQA2	Account	Household Account
			0121I000000nGLKQA2	Account	Organization
	*/
 
 BEGIN -- DROP IMP

	DROP TABLE [safb_migration].[DBO].[IMP_CONTACT]

END 

BEGIN -- CREATE IMP 
					select   [OwnerId] = [safb_migration].[dbo].[fnc_OwnerId]()     
							,[AccountId] = [xa].[Id]
							,[Legacy_Id__c] = [t1].[Legacy_Id__c] -- Set Legacy Id = "DQ-" + [ID_NUM]	
							,[LastName] = [t1].[LastName]		
							,[FirstName] = [t1].[FirstName]		
							,[MiddleName] = [t1].[Middle_Name]		
							,[Salutation] = [safb_migration].[dbo].[proper]([t1].[Title])
							,[Suffix] = [t1].[Suffix]		
							,[npsp__Exclude_from_Household_Formal_Greeting__c] ='TRUE'
							,[npsp__Exclude_from_Household_Informal_Greeting__c] ='TRUE'
 							,[MailingStreet] = Concat( [t1].[Address]	, Char(13), [t1].[Address2])		
							,[MailingCity] = [t1].[City]		
							,[MailingState] = [t1].[State]		
							,[MailingPostalCode] = [t1].[Zip]		
							,[County__c] = [t1].[County]		
							,[MailingCountry] = [t1].[stgCountry]		
							,[HomePhone] = [t1].[Telephone]		
							,[npe01__WorkPhone__c] = [t1].[Work_Phone]		
							,[Fax] = [t1].[Fax_Phone]		
							,[MobilePhone] = [t1].[Mobile_Phone]		
							,[npe01__PreferredPhone__c] = case when [t1].[Telephone] is not null then 'Home' 
															 when [t1].[Work_Phone] is not null then 'Work'
															 when [t1].[Mobile_Phone] is not null then 'Mobile'
															 else null end 
							,[npe01__Preferred_Email__c] = Iif([t1].[EMail] is null, null, 'Personal')
							,[Email] = [t1].[EMail]		
							,[Gender__c] = case [t1].[Gender]	when 'M' then 'Male' when 'F' then 'Female' else null end
							,[Birthdate] = [t1].[Birthday]		
							,[Deceased_Date__c] = [t1].[Deceased_Date]	
							,[npsp__Deceased__c] = Iif([t1].[Deceased_Date] is not null, 'TRUE','FALSE')
							,[Marital_Status__c] = [ms].[CODE_DESCRIPTION] -- D: Divorced M: Married S: Single U W: Widowed	
							,[Title] = [t1].[Job_Title]		
							,[CreatedDate] =  Cast([t1].[When_Donor_Added] as Date)
							,[Annual_Giving_Score__c] = [t1].[Annual]		
							,[Donor_Profile_BBTA__c] = [t1].[Donor_Profile_BBTA]		
							,[Major_Giving_Score__c] = [t1].[Major_Giving]		
							,[Planned_Giving_Score__c] = [t1].[Planned_Giving]		
							,[Target_Gift_Range__c] = [t1].[Target_Gift_Range]		
							,[Donor_Velocity_Rating__c] = [t1].[Velocity_Ratings]		
							,[Data_Source__c] = 'Donor Quest'		-- "Donor Quest"					 
							,[Less_Mail__c] = case when [t1].[Club]='LESS MAIL' then 'true' else 'false' end  --see chart_club. 
							,[RecordTypeId] = Iif([t1].[stgRecordTypeName] ='Household','0120U0000004RNUQA2','0120U0000004RNZQA2')

							,[Board_Status__c] = iif([t1].[Club]='BOARD LEAD','Current', null)

							--reference
							,[t1].[stgRecordTypeName] as [zrefRecordType]
							,[t1].[ID_Num] as [zrefIDNum]
							,'primary contact' [zrefContactType]
			 		into [safb_migration].[DBO].[IMP_CONTACT]
			
					from [safb_dq].[dbo].[tblHeader] as [t1]
					inner join [safb_migration].[dbo].[xtr_account] as [xa] on [t1].[legacy_id__c] = [xa].[legacy_id__c]
				    left join [safb_dq].[dbo].[Marital_Status] as [ms] on [t1].[Marital_Status]=[ms].[CODE]
					 left join [safb_dq].[dbo].[CHART_Club] as [cc] on [t1].[Club] = [cc].[Club]
					where [t1].[LastName] is not null  and [t1].[LastName]<>''
				union
						--secondary contact
				 		select   [OwnerId] = [safb_migration].[dbo].[fnc_OwnerId]()     
							,[AccountId] = [xa].[Id]
							,[Legacy_Id__c] = [t1].[Legacy_Id__2] -- Set Legacy Id = "DQ-" + [ID_NUM]	
							,[LastName] = [t1].[Secondary_Last_Name]		
							,[FirstName] = [t1].[Secondary_First_Name]		
							,[Middle_Name] = [t1].[Secondary_Middle_Name]		
							,[Salutation] = [safb_migration].[dbo].[proper]([t1].[Secondary_Title])
							,[Suffix__c] = [t1].[Secondary_Suffix]		
							,[npsp__Exclude_from_Household_Formal_Greeting__c] ='TRUE'
							,[npsp__Exclude_from_Household_Informal_Greeting__c] ='TRUE'
							,[MailingStreet] = Concat( [t1].[Address]	, Char(13), [t1].[Address2])		
							,[MailingCity] = [t1].[City]		
							,[MailingState] = [t1].[State]		
							,[MailingPostalCode] = [t1].[Zip]		
							,[County__c] = [t1].[County]		
							,[MailingCountry] = [t1].[stgCountry]		
							,[HomePhone] = [t1].[Secondary_Telephone]		
							,[npe01__WorkPhone__c] = [t1].[Secondary_Work_Phone]		
							,[Fax] = [t1].[Secondary_Fax_Phone]		
							,[MobilePhone] = [t1].[Secondary_Mobile_Phone]		
							,[npe01__PreferredPhone__c] = case when [t1].[Secondary_Telephone] is not null then 'Home' 
									when [t1].[Secondary_Work_Phone] is not null then 'Work'
									when [t1].[Secondary_Mobile_Phone] is not null then 'Mobile'
									else null end 
							,[npe01__Preferred_Email__c] = Iif([t1].[Secondary_EMail] is null, null, 'Personal')
							,[Email] = [t1].[Secondary_EMail]		
							,[Gender__c] = case [t1].[Secondary_Gender] 	when 'M' then 'Male' when 'F' then 'Female' else null end		
							,[Birthdate] = [t1].[Secondary_Birth_Date]		
							,[Deceased_Date__c] = [t1].[Secondary_Deceased_Date]		
							,[npsp__Deceased__c] = case when [t1].[Secondary_Deceased_Date] is not null then 'TRUE' else 'FALSE' end
							,[Marital_Status__c]= null  --filler
							,[Title] = null --filler
							,[CreatedDate] = Cast([t1].[When_Donor_Added] as Date)
							,[Annual_Giving_Score__c] = null
							,[Donor_Profile_BBTA__c] = null
							,[Major_Giving_Score__c] = null
							,[Planned_Giving_Score__c] = null
							,[Target_Gift_Range__c] = null
							,[Donor_Velocity_Rating__c] = null
							,[Data_Source__c] = 'Donor Quest'		-- "Donor Quest"					 
							,[Less_Mail__c] =  null --filler
							,[RecordTypeId] = Iif([t1].[stgRecordTypeName] ='Household','0120U0000004RNUQA2','0120U0000004RNZQA2')
							,[Board_Status__c] = null
							--reference
							,[t1].[stgRecordTypeName] as [zrefRecordType]
							,[t1].[ID_Num] as [zrefIDNum]
							,'secondary contact' [zrefContactType]
			 	
					from [safb_dq].[dbo].[tblHeader] as [t1]
					inner join [safb_migration].[dbo].[xtr_account] as [xa] on [t1].[legacy_id__c] = [xa].[legacy_id__c]
				    left join [safb_dq].[dbo].[Marital_Status] as [ms] on [t1].[Marital_Status]=[ms].[CODE]
					where [t1].[Secondary_Last_Name] is not null   --secondary last name
				 



END -- TC1: 90,633

BEGIN -- AUDIT


	SELECT * FROM [safb_migration].[DBO].[IMP_CONTACT]
	WHERE [Legacy_ID__c] IN (SELECT [Legacy_ID__c] FROM [safb_migration].[DBO].[IMP_CONTACT] group BY [Legacy_ID__c] HAVING COUNT(*)>1)
	ORDER BY [Legacy_ID__c]

  	SELECT COUNT(*) FROM [safb_migration].[DBO].[IMP_CONTACT]


	select * from [safb_migration].[DBO].[IMP_CONTACT]
	where [IMP_CONTACT].[LastName] is null
	 or [IMP_CONTACT].[AccountId] is null 
	 or [CreatedDate] is null 
	 --or [IMP_CONTACT].[LastName]='Unknown'

END 

begin --exceptions

	select [ErrorMessage], count(*) [c] 
	from [safb_migration].[dbo].[imp_contact_xerror1]
	group by [ErrorMessage]

		select * from [safb_migration].[dbo].[imp_contact_xerror1]
		--drop old error cols
		alter table [safb_migration].[dbo].[imp_contact_xerror1]
		drop column [ErrorCode], [ErrorColumn]
		--remame old error col so it doesn't interfere on next upload
		exec [sp_rename] 'safb_migration.dbo.imp_contact_xerror1.ErrorMessage', 'OldErrorMessage', 'COLUMN';

	select [OldErrorMessage], [Legacy_Id__c], [EMail], [FirstName], [LastName]--birthdate, deceased_Date__c, cast(deceased_date__c as date) testdate
	from [safb_migration].[dbo].[imp_contact_xerror1]
	where [OldErrorMessage]  like '%emai%'  
	order by [OldErrorMessage] desc
	
	update [safb_migration].[dbo].[imp_contact_xerror1]
	set [email]=null
	where [OldErrorMessage] like 'INVALID_EMAIL_ADDRESS:Email: invalid email address%'
	--147

	update [safb_migration].[dbo].[imp_contact_xerror1]
	set [Deceased_Date__c] = cast([deceased_Date__c] as date)
	where [deceased_Date__c] is not null
	--609
	update [safb_migration].[dbo].[imp_contact_xerror1]
	set [Birthdate]= cast([Birthdate] as date)
	where [Birthdate] is not null
	--17


	select *
	from [safb_migration].[dbo].[imp_contact_xerror2]
	order by [ErrorMessage] desc


	
	select [ic].*
	from [safb_migration].[dbo].[IMP_CONTACT] as [ic]
	left join [safb_migration].[dbo].[xtr_contact] as [xc] on [ic].[Legacy_Id__c]=[xc].[Legacy_Id__c]
	where [xc].[legacy_id__c] is null


	

end
