USE [safb_dq]
GO

--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
			0121I000000nGLJQA2	Account	Household Account
			0121I000000nGLKQA2	Account	Organization
	*/
  
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_VOL_CONTACT

END 

BEGIN -- tblCervis contacts
			select  distinct
						 OwnerId = [safb_migration].[dbo].[fnc_OwnerId]() 
						,Legacy_Id__c = 'CERVIS-'++Cast(t1.CERVIS_ID as Varchar(20)) 
						,FirstName = t1.First_Name					-- Do not migrate Acct/Contact where First_Name like "Group Participants"
						,MiddleName = Cast('' as Varchar(30))						--filler
						,LastName = t1.Last_Name	
						,Suffix = ''							--filler
						,npe01__HomeEmail__c = t1.[E_mail_Address]	
						,npe01__Preferred_Email__c  =  'Personal'
						,HomePhone = t1.Primary_Phone	
						,npe01__PreferredPhone__c = 'Home'

						,Birthdate =Iif([t1].Birth_Date is null ,null, [t1].Birth_Date )  
						,MailingStreet = Concat(t1.[Address], Char(13), t1.Line_2)
						,MailingCity = t1.CityTown	
						,MailingState = t1.StTerrProv	
						,MailingPostalCode = t1.ZipPost_Code	
						,MailingCountry = case when t1.[Address] is not null then 'United States' else null end	-- OK to default to "United States" as with DonorQuest 
						,Emergency_Contact__c = LTrim(RTrim(Left([t1].[Emergency_Contact], CHARINDEX('-', [t1].[Emergency_Contact])-1)))  -- Split name and phone number
						,Emergency_Contact_Phone__c = LTrim(RTrim(Substring([t1].[Emergency_Contact], CHARINDEX('-', [t1].[Emergency_Contact])+1, len([t1].[Emergency_Contact])-(CHARINDEX('-', [t1].[Emergency_Contact])-1))))    -- Split name and phone number
						,Liability_Waiver_Medical_Release_on_file__c = case t1.Liability_WaiverMedical_Release_on_file	when 'No' then 'False' when 'Yes' then 'True' else 'False' end
						,Terms_Conditions__c =	Iif([t1].Terms___Conditions is null or IsDate(t1.Terms___Conditions) =0,null, Cast([t1].Terms___Conditions as DateTime))  
						,CreatedDate = t1.Account_Creation_Date	
						,Interest_Categories__c = t1.Interest_Categories	-- Replace "&amp;" with "&"
						,Data_Source__c = 'Cervis volunteers'	-- "Cervis volunteers"
					  ----reference
						  ,zrefFullName = Concat(t1.[First_Name], ' ' , [t1].[Last_Name])
						  ,zrefLegacyID = t1.[CERVIS_ID]

					into [safb_migration].DBO.IMP_VOL_CONTACT
					from [safb_dq].dbo.[tblVolCervis] as [t1]
					where t1.[First_Name] not  like '%Group Participant%'   -- Do not migrate Acct/Contact where First_Name like "Group Participants"

				union all 
		--tblVolunteerGroupLeader contacts
 				select     OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 						  ,Legacy_Id__c = 'VGL-' +Cast([t1].[Pri_Group_Contact_CERVIS_ID] as Varchar(20))
						  ,FirstName = t1.[FirstName]
						  ,MiddleName = t1.[MiddleName]
						  ,LastName = t1.[LastName]
						  ,Suffix = t1.[Suffix]
						  ,npe01__HomeEmail__c = t1.[Pri_Group_Contact_E_mail_Address]
						  ,npe01__Preferred_Email__c = 'Personal'
						  ,HomePhone = t1.[Pri_Group_Contact_Primary_Phone]
						  ,npe01__PreferredPhone__c = 'Home'
							--filler
							,Birthdate = null
							,MailingStreet = null
							,MailingCity = null
							,MailingState = null
							,MailingPostalCode = null
							,MailingCountry = null
							,Emergency_Contact__c = null
							,Emergency_Contact_Phone__c = null
							,Liability_Waiver_Medical_Release_on_file__c = 'False'
							,Terms_Conditions__c = null
							,CreatedDate = null
							,Interest_Categories__c = null

  				 		  ,Data_Source__c = 'Vol Group Leader'

						  ----reference
						  ,zrefFullName = t1.[Pri_Group_Contact_Name]
						  ,zrefLegacyID =t1.[Pri_Group_Contact_CERVIS_ID]

 	 				from [safb_migration].dbo.stgGroupLeaderContacts as T1
				 union all
			--tblVolSignIn contact
 				select     distinct  OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     			
							,Legacy_Id__c = 'VOLS-'+Cast(t1.Entry_Id as Varchar(20))
							,FirstName = [safb_migration].dbo.proper(t1.[Name])
							,Middle = null
							,LastName = [safb_migration].dbo.proper(t1.[last])
							,Suffix = null 
							,npe01__HomeEmail__c = t1.Email	
							,npe01__Preferred_Email__c = Iif(t1.email is null, null, 'Personal')
							,HomePhone = LTrim(RTrim(Str(t1.Phone, 20)))
							,npe01__PreferredPhone__c = Iif(t1.[Phone] is null, null, 'Home')
							--filler
								,Birthdate = null
								,MailingStreet = null
								,MailingCity = null
								,MailingState = null
								,MailingPostalCode = null
								,MailingCountry = null
								,Emergency_Contact__c = null
								,Emergency_Contact_Phone__c = null
							,Liability_Waiver_Medical_Release_on_file__c = Iif(t1.VOLUNTEER_NOTICE is null, 'False', 'True')
								,Terms_Conditions__c = null
							,CreatedDate = t1.Date_Created	
								,Interest_Categories__c = null 
	 				 	
							,Data_Source__c = 'VolunteerSign-in' -- "Volunteers sign-in"	 

						  ----reference
						  ,zrefFullName = Concat(t1.[Name], ' ' , t1.[Last])
						  ,zrefLegacyID = t1.[Entry_Id] 
 	 				
					from [safb_dq].dbo.[tblVolWufoo] as t1

END -- TC1: 52984



BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_VOL_CONTACT
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_VOL_CONTACT group BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_VOL_CONTACT 
	 
	select count(*) from [safb_dq].dbo.[tblVolCervis] as [tvc]
	select count(*) from [safb_dq].dbo.[tblVolGroupLeader] as [tvgl]
	select count(*) from [safb_dq].dbo.[tblVolWufoo]  as [tvsi]

	select * from [safb_migration].dbo.[IMP_VOL_CONTACT] as [ivc] 
	where [ivc].[LastName] is null

	select Birthdate, isdate(cast(left(cast([ivc].[Birthdate] as nvarchar(20)),10) as date)) ib 
	from [safb_migration].[dbo].[IMP_VOL_CONTACT] as [ivc]
	where [ivc].[Birthdate] is not null



	select distinct [t1].[Birth_Date], IsDate(t1.[Birth_Date]) checkdate	
			,Birthdate =Iif([t1].Birth_Date is null or IsDate(t1.[Birth_Date]) =0,null, Cast([t1].Birth_Date as DateTime)) 
			,Terms_Conditions__c =	Iif([t1].Terms___Conditions is null or IsDate(t1.Terms___Conditions) =0,null, Cast([t1].Terms___Conditions as DateTime))  
	from [safb_dq].dbo.[tblVolCervis] as [t1]
	order by checkdate	



end 



select * from [safb_migration].dbo.xerror_vol_contact_2



begin--error/exceptions
			
				select   [ivc].*
				into	[safb_migration].dbo.IMP_VOL_CONTACT_xcp
				from     [safb_migration].[dbo].[IMP_VOL_CONTACT]           as [ivc]
						 left join [safb_migration].[dbo].[xtr_vol_contact] as [x]
								   on [ivc].[Legacy_Id__c] = [x].[Legacy_Id__c]
				where    [x].[Legacy_Id__c] is null
				order by [ivc].[Legacy_Id__c];
				--41769



				select * from [safb_migration].dbo.xerror_vol_contact_2
				
					update [safb_migration].dbo.xerror_vol_contact_2 
					set [FirstName] ='Ron' where [xerror_vol_contact_2].[ErrorMessage] like 'First%'

					update [safb_migration].dbo.xerror_vol_contact_2 
					set [xerror_vol_contact_2].[npe01__HomeEmail__c]=null 
					where [xerror_vol_contact_2].[ErrorMessage] like 'Personal%'


				--audit complete upload

				select * from [safb_migration].dbo.[IMP_VOL_CONTACT] as [ivc]
				left join [safb_migration].dbo.[xtr_contact] as [xc] on [ivc].[Legacy_Id__c] = [xc].[Legacy_Id__c]
				where xc.[Legacy_Id__c] is null

end
