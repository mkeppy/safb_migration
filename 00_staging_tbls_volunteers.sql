-- CERVIS datbase
	/*2018.10.29 INTEREST CATEGORIES. need to convert from multivalue field to single column field for clean up. After cleanup/tranformation, 
	// need to convert back to multipicklist.
	//2018.10.30. SAFB comment: will convert values as is, no need to clean up.
	//2018.10.31. MK: replace "&amp;" with "&" and replace "," with ";" to make the field a multipicklist.
	*/

		--cleanup "&amp;"

			select distinct interest_Categories 
			from [safb_dq].[dbo].[tblVolCervis] 
			where [Interest_Categories] like '%&amp;%'

			update [safb_dq].[dbo].[tblVolCervis] 
			set [Interest_Categories] = Replace([Interest_Categories],'&amp;','&')
			where [Interest_Categories] like '%&amp;%'

		--replace "," with ";"
			update [safb_dq].[dbo].[tblVolCervis] 
			set [Interest_Categories] = Replace([Interest_Categories],',',';')
			where [Interest_Categories] like '%,%'

			--test
			select distinct [Interest_Categories]
  			from [safb_dq].[dbo].[tblVolCervis]
			where [Interest_Categories] is not null
			group by [Interest_Categories]
			order by [Interest_Categories] asc

	--ACTIVITY ADDRESS parsing. manual work
	/* Activity_Address_location needs to be parsed into street, city, state, zip. 
	// Create list and assign unique Id to unique values using dense_rank() function. 
	// Export to Excel and manually parse table
	// Import results to SQL. 
	*/
			select distinct [Activity_Location_Address] 
			from [safb_dq].[dbo].[tblVolCervis]
			order by [Activity_Location_Address] 

		--add sequence field	
			alter table [safb_dq].dbo.[tblVolCervis] 
			add ActAddrSeq Char(2)
	
		--test select
			select distinct [Activity_Location_Address] , ActAddrSeq,
			Dense_Rank() over( order by [Activity_Location_Address] ) seq
 			from [safb_dq].[dbo].[tblVolCervis]
			where [Activity_Location_Address]  is not null
			group by [Activity_Location_Address] , ActAddrSeq
			order by [seq] asc

		--update ActAddrSeq
			update [safb_dq].dbo.[tblVolCervis] 
			set ActAddrSeq = t2.seq
			from  [safb_dq].[dbo].[tblVolCervis] as  t1 
			inner join (select distinct [Activity_Location_Address] , ActAddrSeq,
						Dense_Rank() over( order by [Activity_Location_Address] ) seq
 						from [safb_dq].[dbo].[tblVolCervis]
						where [Activity_Location_Address]  is not null) as t2 on t1.[Activity_Location_Address]=t2.[Activity_Location_Address]
			where t1.[Activity_Location_Address] is not null
					
		--Run query. Copy/Past in CSV file. Set colum A and B as TEXT data type in CSV to remove carriage return/line feed characters
		-- to help use textToColumn to parse address. 
					select [Activity_Location_Address], ActAddrSeq
					from [safb_dq].[dbo].[tblVolCervis]
					where [Activity_Location_Address] is not null
					group by [Activity_Location_Address], ActAddrSeq
					order by Cast(ActAddrSeq as Int) asc


	--Activity_Time:  need to parse Activity Time into StartDate, EndDate, and Duration=(EndDate - StartDate)
	
		--sample code. 
			declare @Date01 time= '8:30 AM',  @Date02 time= '3:00 PM'
			select  DATEDIFF(SECOND,@Date01 ,@Date02 )*1.00  / (60*60)

		--add new fields.
			alter table [safb_dq].[dbo].[tblVolCervis]
			add StartTime Varchar(20) null, EndTime Varchar(20) null , Duration Varchar(20) null;

		--test time parse
			select distinct [Activity_Time] , 
			LTrim(RTrim(Substring([Activity_Time],1,CharIndex(' - ', [Activity_Time])))) StartTime,
			LTrim(RTrim(substring([Activity_Time], CHARINDEX('-', [Activity_Time])+1, len([Activity_Time])-(CHARINDEX('-', [Activity_Time])-1)))) EndTime
			,duration=DATEDIFF(SECOND,LTrim(RTrim(Substring([Activity_Time],1,CharIndex(' - ', [Activity_Time])))),LTrim(RTrim(substring([Activity_Time], CHARINDEX('-', [Activity_Time])+1, len([Activity_Time])-(CHARINDEX('-', [Activity_Time])-1)))) )*1.00  / (60*60)
			from [safb_dq].[dbo].[tblVolCervis]
			where activity_time is not null
			order by [Activity_Time]

		--update time fields
			update [safb_dq].dbo.[tblVolCervis]
			set StartTime=LTrim(RTrim(Substring([Activity_Time],1,CharIndex(' - ', [Activity_Time])))) ,
				EndTime=LTrim(RTrim(substring([Activity_Time], CHARINDEX('-', [Activity_Time])+1, len([Activity_Time])-(CHARINDEX('-', [Activity_Time])-1)))),
			    Duration=DATEDIFF(SECOND,LTrim(RTrim(Substring([Activity_Time],1,CharIndex(' - ', [Activity_Time])))),LTrim(RTrim(substring([Activity_Time], CHARINDEX('-', [Activity_Time])+1, len([Activity_Time])-(CHARINDEX('-', [Activity_Time])-1)))) )*1.00  / (60*60)
			from [safb_dq].[dbo].[tblVolCervis]
			where activity_time is not null
		
			select * from [safb_dq].dbo.[tblVolCervis]


	--GROUP tbl. Contacts not in Cervis

			DROP TABLE  [safb_migration].dbo.stgGroupLeaderContacts

			--group leader records that do not exist in cervis
			select   distinct 
					 [tvgl].[Pri_Group_Contact_CERVIS_ID]
					,[tvgl].[Pri_Group_Contact_Name]
				    ,[safb_migration].dbo.PARSE_NAME_UDF([tvgl].[Pri_Group_Contact_Name],'F') as FirstName
				    ,[safb_migration].dbo.PARSE_NAME_UDF([tvgl].[Pri_Group_Contact_Name],'M') as MiddleName
				    ,[safb_migration].dbo.PARSE_NAME_UDF([tvgl].[Pri_Group_Contact_Name],'L') as LastName 
					,[safb_migration].dbo.PARSE_NAME_UDF([tvgl].[Pri_Group_Contact_Name],'s') as Suffix
					,[tvgl].[Pri_Group_Contact_E_mail_Address]
					,[tvgl].[Pri_Group_Contact_Primary_Phone]
					,'VGL-' +Cast([tvgl].[Pri_Group_Contact_CERVIS_ID] as Varchar(20)) as Legacy_Id__c
					 
					,tvc.[CERVIS_ID]
			--into [safb_migration].dbo.stgGroupLeaderContacts
			from [safb_dq].dbo.[tblVolGroupLeader] as [tvgl]
			left join [safb_dq].dbo.[tblVolCervis] as [tvc] on [tvgl].[Pri_Group_Contact_CERVIS_ID]=tvc.[CERVIS_ID]
			where [tvc].[CERVIS_ID] is null
		ORDER BY [tvgl].[Pri_Group_Contact_CERVIS_ID];


			select * from [safb_dq].dbo.tblVolCervis order by cervis_id

			select * from [safb_migration].dbo.stgGroupLeaderContacts
			 
--tblCervis campaign
		alter table [safb_dq].dbo.[tblVolCervis]
		add myId Int identity(1,1);
		 
		DROP TABLE  [safb_migration].DBO.stgCervisCampId

		select   t1.[Activity_ID]
				,t1.[Activity_ID] as CampLegId
				,t1.[Activity_Name] 
				,t1.[CERVIS_ID]
				,Len(t1.[Activity_Name]) as zrefNameLen
				,[T1].[myId]
		into [safb_migration].DBO.stgCervisCampId
		from [safb_dq].dbo.[tblVolCervis]  as T1
		order by t1.[Activity_ID], t1.[Activity_Name]

		select * from [safb_migration].dbo.[stgCervisCampId] as [scci]
		order by camplegId
		 


--tblVolSignIn campaign
		select  t1.[Volunteer_Registration_Location]
				,dense_rank() over (order by t1.[Volunteer_Registration_Location]) as CampLegId
				,t1.[Entry_Id]
				,Len(t1.[Volunteer_Registration_Location]) as zrefNameLen
		
		into [safb_migration].DBO.stgVolSignInCampId
		from [safb_dq].dbo.tblVolWufoo as T1
		order by t1.[Volunteer_Registration_Location]

		select * from [safb_migration].dbo.stgVolSignInCampId as [scci]
		order by camplegId
		




---[tblVolCervisTimeSlots]	
	alter table [safb_dq].[dbo].[tblVolCervisTimeSlots]
	add EVDate nvarchar(10), EVTime nvarchar(10)

	update [safb_dq].[dbo].[tblVolCervisTimeSlots]
	set EVDate = convert(nvarchar(10), Event_Slot_Start_Date, 101),
	EVTime = replace(replace(convert(nvarchar(10), convert(time, Event_Slot_Start_Date, 100),100),'PM', ' PM'),'AM',' AM') 

