use [safb_dq]
go

select distinct Donor_Profile_BBTA  
from dbo.[tblHeader] as [th]


select distinct Velocity_Ratings
from dbo.[tblHeader] as [th]

select distinct Target_Gift_Range
from dbo.[tblHeader] as [th]


select distinct [g].[CODE_DESCRIPTION]
from dbo.[Gender] as [g] 


select distinct [ms].[CODE_DESCRIPTION]
from dbo.[Marital_Status] as [ms] 

select [tvc].[Interest_Categories] 
from [dbo].[tblVolCervis] as [tvc]

select distinct group_type
from [dbo].[tblVolGroupLeader]

select distinct [tblDonation].[Donation_Account]
from [dbo].[tblDonation]

select * from [dbo].[Donation_TY_Code] as [dtc]

select distinct [tvc].[Activity_Primary_Interest_Category] 
from [dbo].[tblVolCervis] as [tvc]




use [database]
		GO

		/*generate master list of picklist values. 
		  Use table __T1_ALL_FIELDS to get list of piclist field, and use 'with' code to turn columns into rows
			---src: https://stackoverflow.com/questions/18026236/sql-server-columns-to-rows
		*/
		--1) pull master list of objects and fields. 
				SELECT DISTINCT SF_Object, SF_Field_API, ',t.'+SF_Field_API AS 'script'
				FROM [datamap_database].DBO.__T1_ALL_FIELDS 
				WHERE  Data_Type LIKE '%PICK%'
				GROUP BY SF_Object, SF_Field_API, Data_Type
				ORDER BY SF_Object, SF_Field_API
	 
		--2) picklist values. select list of fields from qry above and copy/paste here.. 
			 --copy/paste object name to replace in IMP_XXXXXXXX and in 'XXXXXXXX' ObjectName

		;with CTE1 as (
		select  
		(select  

		t.Country__c


		FOR xml raw('row'), type) as Data
		from [datamap_database].[dbo].[IMP_ZIPCODE] AS t		/*REPLACE IMP_XXXXXXX */
		), CTE2 as (
		SELECT
 				F.C.value('local-name(.)', 'nvarchar(128)') as FieldName,
				F.C.value('.', 'nvarchar(max)') as FieldValue
		FROM CTE1 AS c
			outer apply c.Data.nodes('row/@*') as F(C)
		) SELECT DISTINCT 
		--'' AS ObjectName,
		CTE2.FieldName	 AS 'ZIP CODE FieldName',		/*REPLACE IMP_XXXXXXX */
		CTE2.FieldValue  AS 'ZIP CODE FieldValue'		/*REPLACE IMP_XXXXXXX */
		FROM CTE2 
		WHERE CTE2.FieldName IS NOT NULL AND FieldValue IS NOT NULL 
		ORDER BY FieldName, FieldValue

  