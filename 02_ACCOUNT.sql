USE [safb_dq]
GO

--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
			0121I000000nGLJQA2	Account	Household Account
			0121I000000nGLKQA2	Account	Organization
	*/
 
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_ACCOUNT

END 

BEGIN -- CREATE IMP 

					select   OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 							,RecordTypeId = case when t1.[stgRecordTypeName]='Organization' then '0121I000000nGLKQA2' 
												 when t1.stgRecordTypeName='Household' then '0121I000000nGLJQA2' else null end    --LOOKUP: Lookup (Recordtype)	
							,[Name] = LTrim(case when t1.[stgRecordTypeName]='Organization' then t1.[OrgName] 
												 when t1.stgRecordTypeName='Household' then Concat(t1.[FirstName],' ' , t1.[LastName]) else null end )	-- Use First/Last as Account Name if RecordType = Household	
						    ,[Type] = t1.[stgAccountType]
							,npo02__Informal_Greeting__c = t1.Salutation2		
							,npo02__Formal_Greeting__c = t1.Salutation1		
							,Super_Informal_Greeting__c = t1.Salutation3	
							,Legacy_Informal_Greeting__c = t1.Salutation2		
							,Legacy_Formal_Greeting__c = t1.Salutation1			
							,Legacy_Id__c = t1.Legacy_ID__c	-- Set Legacy Id = "DQ-" + [ID_NUM]	
							,Donor_ID__c = t1.[ID_Num]	
							,BillingStreet = Concat( t1.[Address]	, Char(13), t1.[Address2])
 							,BillingCity = t1.City		
							,BillingState = t1.[State]		
							,BillingPostalCode = t1.Zip		
							,County__c = t1.County		
							,BillingCountry = t1.stgCountry		
							,Active__c = case t1.Inactive when 'N' then 'TRUE' else 'FALSE' end
							,Bad_Address__c = case t1.Bad_Address when 'Y' then 'TRUE' else 'FALSE' end
							,Do_Not_Solicit__c = case t1.Do_Not_Solicit	when 'Y' then 'TRUE' else 'FALSE' end	
							,Do_Not_Acknowledge__C = case t1.Do_Not_Acknowledge	when 'Y' then 'TRUE' else 'FALSE' end
							,Website = t1.Web_Address		
							,Legacy_Donor_Type__c = t1.Donor_Type		
							,Best_Fiscal_Year__c = t1.Best_Fiscal_Year		
							,Best_Fiscal_Year_Total__c = t1.Best_Fiscal_Year_Total		
							,CreatedDate = t1.When_Donor_Added		
							,Annual_Giving_Score__c = t1.Annual		
							,Donor_Profile_BBTA__c = t1.Donor_Profile_BBTA		
							,Major_Giving_Score__c = t1.Major_Giving		
							,Planned_Giving_Score__c = t1.Planned_Giving		
							,Target_Gift_Range__c = t1.Target_Gift_Range		
							,Donor_Velocity_Rating__c = t1.Velocity_Ratings		
						    ,Primary_Relationship_Manager__c = xu.[Name]
						
							,Data_Source__c = 'Donor Quest'		-- "Donor Quest"		
							
							--reference
							, t1.[ID_Num] as zrefIDNum
							, t1.[stgRecordTypeName] as zrefRecordTypeName

			 			into [safb_migration].DBO.IMP_ACCOUNT
					    from [safb_dq].dbo.[tblHeader] as t1
						left join (select * from [safb_dq].dbo.[CHART_Users] 
											 where [CHART_Users].[SourceField]='Solicitor' and [CHART_Users].[Convert]='Yes') as cu
									on  t1.[Solicitor]=cu.[FieldValue]
						left join [safb_migration].dbo.[xtr_Users] as [xu] on cu.UserName = xu.[Username]
						 
END -- TC1: 72,226,  tc2: 105,268


BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_ACCOUNT
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_ACCOUNT GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_ACCOUNT
	select COUNT(*) from [safb_dq].dbo.[tblHeader] as [th]
	 

	select ia.[RecordTypeId], [ia].[Legacy_Id__c], ia.[Name], th.[FirstName], th.[LastName], th.[OrgName], th.[Donor_Type] 
	from [safb_migration].dbo.[IMP_ACCOUNT] as [ia] 
	left join [safb_dq].dbo.[tblHeader] as [th] on [ia].[Legacy_Id__c] = [th].[Legacy_Id__c]
	where [ia].[Name] is null or ia.[Name]=''
	or [ia].[RecordTypeId] is null

	--uupate last name to add Contacts next. 
	update [safb_dq].dbo.[tblHeader] 
	set [tblHeader].[LastName]='Unknow'
	from [safb_migration].dbo.[IMP_ACCOUNT] as [ia] 
	left join [safb_dq].dbo.[tblHeader] as [th] on [ia].[Legacy_Id__c] = [th].[Legacy_Id__c]
	where [ia].[Name] is null or ia.[Name]=''
	 

	--blank name
	update [safb_migration].dbo.[IMP_ACCOUNT] 
	set [IMP_ACCOUNT].[Name]=concat(th.[FirstName],' ' ,th.[LastName])
	from [safb_migration].dbo.[IMP_ACCOUNT] as [ia] 
	left join [safb_dq].dbo.[tblHeader] as [th] on [ia].[Legacy_Id__c] = [th].[Legacy_Id__c]
	where [ia].[Name] is null 
	
	
	select type, Count(*) c 
	from [safb_migration].dbo.[IMP_ACCOUNT] 
	group by type
	--exceptions
	select * from [safb_dq].dbo.[tblHeader] as [th]
	where [th].[ID_Num] in ('68075','110806','1150995','1374744','1514449')

	
END 


begin--- XTR ACCOUNT

select * from [safb_migration].dbo.imp_account_xerror

update [safb_migration].dbo.imp_account_xerror
set Name = 'Unknown' where name ='' or name is null

alter table [safb_migration].dbo.imp_account_xerror
drop column errorcode, errorcolumn, errormessage

 
truncate table [safb_migration].[dbo].[xtr_account] 


select * from [safb_migration].[dbo].[xtr_account] as [xa]