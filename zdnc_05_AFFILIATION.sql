USE [safb_dq]
GO


--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
			0121I000000nGLJQA2	Account	Household Account
			0121I000000nGLKQA2	Account	Organization
	*/
  
BEGIN -- DROP IMP
	DROP TABLE [safb_migration].DBO.IMP_AFFILIATION
END 
					
begin				/* dnc FB-0003 
					--affiliation 1: first records is Household, second is OrganizatiON
					select   OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()
				 			,Legacy_Id__c = 'LKG-' + Cast(t1.Linkage_Child_ID	as NVarchar(20))
							,npe5__Contact__c =      xc.id	         	--- t1.Linkage_Linked_To	 --LOOKUP: Lookup (Contact)
					 		,npe5__Organization__c = xa.id				--- t1.Linkage_Parent_ID	 --LOOKUP: Lookup (Account)
 							,npe5__Description__c = LTrim(Concat(case when t1.Linkage_Relation_Type is not null then 'Donor Quest Linkage Type: ' + t1.Linkage_Relation_Type else '' end, 
													       case when t1.[Linkage_Comment] is not null then Char(13) + 'Comment: '+t1.[Linkage_Comment] else '' end)) 	-- Concatenate "Description". Populate [Relation_Type.Description] as noted in the REF_dq_linkage_type.
							,CreatedDate = t1.Linkage_When_Added

							--reference
							,zrefParentID = t1.Linkage_Parent_ID
							,zrefLinkedTo = t1.[Linkage_Linked_To]
 							,zrefRecordTypeCnt  = th1.[stgRecordTypeName] 
							,zrefRecordTypeAct  = th2.[stgRecordTypeName] 
							,zrefIDNum = t1.[Linkage_Child_ID]
					
					into [safb_migration].dbo.IMP_AFFILIATION
					
					from [safb_dq].dbo.[tblLinkage] as [t1]
					inner join [safb_dq].dbo.[tblHeader] as [th1] on t1.[Linkage_Linked_To]=th1.[ID_Num]  --hhd
					inner join [safb_dq].dbo.[tblHeader] as [th2] on t1.[Linkage_Parent_ID]=th2.[ID_Num]  --org
				 	inner join [safb_migration].dbo.xtr_contact as xc on th1.legacy_id__c = xc.legacy_id__c
				 	inner join [safb_migration].dbo.xtr_account as xa on th2.legacy_id__c = xa.legacy_id__c
					where th1.[stgRecordTypeName]='Household' and th2.[stgRecordTypeName]='Organization'

					
					union 

					--affiliation 2: first records is Organization, second is Household
					select   OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()
				 			,Legacy_Id__c = 'LKG-' + Cast(t1.Linkage_Child_ID	as NVarchar(20))
							,npe5__Contact__c =      xc.id	         	--- t1.Linkage_Linked_To	 --LOOKUP: Lookup (Contact)
							,npe5__Organization__c = xa.id				--- t1.Linkage_Parent_ID	 --LOOKUP: Lookup (Contact)
							,npe5__Description__c = LTrim(Concat(case when t1.Linkage_Relation_Type is not null then 'Donor Quest Linkage Type: ' + t1.Linkage_Relation_Type else '' end, 
													       case when t1.[Linkage_Comment] is not null then Char(13) + 'Comment: '+t1.[Linkage_Comment] else '' end)) 	-- Concatenate "Description". Populate [Relation_Type.Description] as noted in the REF_dq_linkage_type.
							,CreatedDate =t1.Linkage_When_Added

							--reference
							,zrefParentID = t1.Linkage_Parent_ID
							,zrefLinkedTo = t1.[Linkage_Linked_To]

							,zrefRecordTypeCnt  = th1.[stgRecordTypeName] 
							,zrefRecordTypeAct = th2.[stgRecordTypeName] 
							,zrefIDNum = t1.[Linkage_Child_ID]
					
					from [safb_dq].dbo.[tblLinkage] as [t1]
					inner join [safb_dq].dbo.[tblHeader] as [th1] on t1.[Linkage_Linked_To]=th1.[ID_Num]   --org
					inner join [safb_dq].dbo.[tblHeader] as [th2] on t1.[Linkage_Parent_ID]=th2.[ID_Num]   --hhd
					inner join [safb_migration].dbo.xtr_contact as xc on th2.legacy_id__c = xc.legacy_id__c
					inner join [safb_migration].dbo.xtr_account as xa on th1.legacy_id__c = xa.legacy_id__c
					where th1.[stgRecordTypeName]='Organization' and th2.[stgRecordTypeName]='Household'
					*/

end -- tc1: 2,807


BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_AFFILIATION
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_AFFILIATION group BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_AFFILIATION

	--CHECK ORG RECORD AND CONTACT
	
	
		select * from [safb_dq].dbo.[tblHeader] as [th] where [th].[ID_Num]='127604' or [th].[ID_Num]='110535'


	---affil and relat not converted: tblHeader records don't exist. 
		select
		     [Linkage_Parent_ID]

		   ,  [Linkage_Linked_To]
		   , [Linkage_Parent_ID]
		   , [Linkage_Relation_Type]
		   , ir.[Legacy_Id__c] relatId
		   , ia.[Legacy_Id__c] affilId

		from [tblLinkage] as [t1]
 		left join [safb_migration].dbo.[IMP_AFFILIATION] as [ia] on t1.[Linkage_Child_ID]=ia.[zrefIDNum]
		left join [safb_migration].dbo.[IMP_RELATIONSHIP] as [ir] on t1.[Linkage_Child_ID]=ir.[zrefIDNum]
		where ia.[zrefParentID] is null or ir.[zrefIDNum] is null

				select [th].[ID_Num], [th].[FirstName], [th].[LastName], [th].[OrgName] 
		,[th].[stgRecordTypeName], [th].[stgAccountType]
		from [safb_dq].dbo.[tblHeader] as [th]
		where [th].[ID_Num] in ('30', '54','114710','8309')


END 