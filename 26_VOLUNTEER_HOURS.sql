

					select   distinct
							 OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 							,Contact__c = xc2.id
							,Volunteer_Job__c = [xvj].id
						 	,Volunteer_Shift__c =  xvs.id
							,Hours_Worked__c = t1.[Activity_Hours] 
							,Planned_Start_Date_Time__c = concat(convert(nvarchar(10), t1.[Activity_Date], 101) ,' ', t1.[StartTime])
							,End_Date__c = convert(Varchar(10), t1.Activity_Date,126)
							,Number_of_Volunteers__c = '1'
							,Status__c = 'Completed'
							,CreatedDate = convert(Varchar(10), t1.Activity_Date,126)
							,[LastModifiedDate] = convert(Varchar(10), t1.Activity_Date,126)
					
							,zrefCampLegId = 'CERVIS-'+ Cast(t2.camplegId as Varchar(20))  
 			 				,zrefVolJobLegId = xvj.[Legacy_Id__c]
							,zrefVolShiftLegId = Concat('CERVIS-'+ Cast(t2.camplegId as Varchar(20))  ,'-',Convert(Varchar(10), t1.Activity_Date,126),'T',Convert(Varchar(12), Convert(Time, [t1].[StartTime])),'Z')	

					--	into [safb_migration].DBO.IMP_VOL_VOLUNTEER_HOURS
			
					from [safb_dq].dbo.[tblVolCervis] as T1
					inner join [safb_migration].DBO.stgCervisCampId as t2 on t1.myid=t2.myid
					 join [safb_migration].dbo.[xtr_contact] as [xc2] on  'CERVIS-'+Cast(t1.CERVIS_ID as Varchar(20))  = xc2.[Legacy_Id__c]
				    left join [safb_migration].dbo.xtr_campaign as xc on 'CERVIS-'+ Cast(t2.camplegId as Varchar(20))   = xc.External_Id__c
					 join [safb_migration].dbo.[xtr_volunteer_job] as [xvj] on  'CERVIS-'+ Cast(t2.camplegId as Varchar(20))  =[xvj].[Legacy_Id__c]
					 join [safb_migration].dbo.[xtr_volunteer_shift] as [xvs] on xvs.Legacy_Id__c = Concat('CERVIS-'+ Cast(t2.camplegId as Varchar(20))  ,'-',Convert(Varchar(10), t1.Activity_Date,126),'T',Convert(Varchar(12), Convert(Time, [t1].[StartTime])),'Z')	
			   --test where t1.[CERVIS_ID]='10759'
					order by [zrefVolJobLegId], [zrefVolShiftLegId]


					select * from [safb_migration].DBO.IMP_VOL_VOLUNTEER_HOURS
					 
		 