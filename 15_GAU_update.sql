

			select xg.id, ig.[npsp__Active__c_update] as npsp__Active__c 
			into [safb_migration].dbo.imp_gau_update_active
			from [safb_migration].dbo.[IMP_GAU] as [ig]
			left join [safb_migration].dbo.[xtr_gau] as [xg] on ig.[Name]=xg.[Name]
			where ig.[npsp__Active__c_update] ='false'