USE [safb_migration]
GO
 

--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
			0121I000000nGLJQA2	Account	Household Account
			0121I000000nGLKQA2	Account	Organization
	*/
 
 
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_ALLOCATION

END 

BEGIN -- CREATE IMP 

					select   OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 			 							,npsp__Amount__c = t1.Donation_Amount	
									--dnc: can't upload to more than 1 parent object.	,npsp__Campaign__c = xt.CampaignId --LOOKUP: Lookup (Campaign)
						  	,npsp__General_Accounting_Unit__c = xg.id	 --LOOKUP: Lookup (Npsp__General_Accounting_Unit__C)
 							,npsp__Opportunity__c = xt.id	 --LOOKUP: Lookup (Opportunity)			 
							,npsp__Percent__c = '100'
							,CreatedDate = case when t1.Donation_When_Added	is null 
												then t1.[Donation_When_Last_Edited]  
												else t1.[Donation_When_Added]  end

						 	,t1.[Donation_Child_ID] as zrefDonId
							,t1.[udfGAU_NAME] as zrefGAU
						 
			 		into [safb_migration].DBO.IMP_ALLOCATION
			
					from [safb_dq].dbo.[tblDonation] as T1
		 			inner join [safb_migration].dbo.xtr_opportunity as xt on 'DQ-'+Cast(t1.Donation_Child_ID	as Varchar(30)) =xt.Legacy_Id__c 
				    inner join [safb_migration].dbo.xtr_gau as xg on [t1].[udfGAU_Name]=xg.[Name]
				  order by  xg.id, xt.id
				 
END -- TC1: 228356; tc2: 329566

BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_ALLOCATION
	WHERE [npsp__Opportunity__c] in (SELECT [npsp__Opportunity__c] from [safb_migration].dbo.IMP_ALLOCATION GROUP BY [npsp__Opportunity__c] HAVING COUNT(*)>1)
	ORDER BY [npsp__Opportunity__c]

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_XXXXXXXXXXXX
	 

END 


select * from [safb_migration].dbo.[IMP_ALLOCATION] as [ia] where [ia].[npsp__Opportunity__c]='0060U00000EbJEIQA3'

select [Donation_Child_ID], [donation_Amount] from [safb_dq].dbo.[tblDonation] where donation_Amount <0
select * from [safb_migration].[dbo].[xtr_Opportunity] as [xo] where [xo].[Legacy_Id__c] ='DQ-362847'

select  a.[npsp__Opportunity__c], a.[npsp__General_Accounting_Unit__c], a.[npsp__Amount__c] , a.[ErrorMessage]
from [safb_migration].dbo.xerror_allocation as [a]
order by a.[ErrorMessage]



select * from [safb_migration].dbo.imp_allocation_xerror1 


where [xerror_allocation].[ErrorMessage] like 'allocation totals can%'


select * from [safb_migration].dbo.[IMP_ALLOCATION] where [IMP_ALLOCATION].[npsp__Amount__c] < 0 
select * from [safb_migration].dbo.[IMP_OPPORTUNITY] as [io] where [io].[Amount] <0
select * from [safb_migration].dbo.[IMP_PAYMENT] as [ip] where [ip].[npe01__Payment_Amount__c] <0

select [td].[Donation_Child_ID], [td].[Donation_Amount]
,[td].[Donation_Fund] 
from [safb_dq].[dbo].[tblDonation] as [td]
where [td].[Donation_Amount] <0
order by [td].[Donation_Amount]



select
[t1].[Donation_Child_ID]
,[t1].[Donation_Parent_ID]  
,[xa].id
					
from [safb_dq].dbo.[tblDonation] as t1
left join [safb_dq].dbo.[tblHeader] as [th] on t1.Donation_Parent_ID=th.[ID_Num]
left join [safb_migration].dbo.xtr_account as xa on th.[Legacy_Id__c]=xa.legacy_id__c
where [th].[ID_Num] is null



select
[t1].[Donation_Parent_ID]  
,Count(*) c
					
from [safb_dq].dbo.[tblDonation] as t1
left join [safb_dq].dbo.[tblHeader] as [th] on t1.Donation_Parent_ID=th.[ID_Num]
left join [safb_migration].dbo.xtr_account as xa on th.[Legacy_Id__c]=xa.legacy_id__c
where [th].[ID_Num] is null
group by [t1].[Donation_Parent_ID]













































































