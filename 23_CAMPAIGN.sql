USE [safb_dq]
GO

--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
			0121I000000nGLJQA2	Account	Household Account
			0121I000000nGLKQA2	Account	Organization
	*/
  
BEGIN -- DROP IMP
	DROP TABLE [safb_migration].DBO.IMP_VOL_CAMPAIGN

END 

BEGIN -- CREATE IMP 
					select   distinct 
					         OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 		 	  				,[Name] = 	Min(t1.[Activity_Name]) 
							,[External_Id__c] = 'CERVIS-'+ Cast(t1.camplegId as Varchar(20))  
							,[RecordTypeId] = '0120U0000004RE8QAM'
					into [safb_migration].DBO.IMP_VOL_CAMPAIGN
 					from [safb_migration].dbo.[stgCervisCampId]  as T1
					group by t1.camplegId
					 --	order by [External_Id__c], [Name]
			
				union 
				
					select   distinct 
					         OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 		 	  				,[Name] = 	t1.[Volunteer_Registration_Location]
							,[External_Id__c] = 'VOLSI-'+ Cast(t1.camplegId as Varchar(20))  
							,[RecordTypeId] = '0120U0000004RE8QAM'
					from [safb_migration].dbo.[stgVolSignInCampId]  as T1
END -- TC1: 364  tc2: 403


BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_VOL_CAMPAIGN
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_VOL_CAMPAIGN group BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_VOL_CAMPAIGN
	 
	 select *, Len([ivc].[Name]) ln 
	 from [safb_migration].dbo.[IMP_VOL_CAMPAIGN] as [ivc]
	 order by ln desc

END 