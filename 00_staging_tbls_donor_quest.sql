use [safb_migration]
go


 
--/DONOR QUEST**************************
	
	
	begin--tbl header: check for data glith
				select * from safb_dq.dbo.[tblHeader] 
				where [ID_Num] is null
				order by [ID_Num]
			
				delete safb_dq.dbo.[tblHeader]
				where [ID_Num] is null
	end 


	begin --update country  tbl HEader

				 SELECT [STATE] , [Country], Count(*) c 
				 from  safb_dq.dbo.[tblHeader] as [th]
				 where 
				 ([STATE] ='AL' or [STATE]='AK' or [STATE]='AZ' or [STATE]='AR'  or [STATE]='CA' or [STATE]='CO' or [STATE]='CT' or [STATE]='DE' 
				 OR [STATE]='DC' or [STATE]='FL'
				 or [STATE]='GA' or [STATE]='HI' or [STATE]='ID' or [STATE]='IL' or [STATE]='IN' or [STATE]='IA' or [STATE]='KS' or [STATE]='KY' 
				 OR [STATE]='LA' or [STATE]='ME' or [STATE]='MD'
				 or [STATE]='MA' or [STATE]='MI' or [STATE]='MN' or [STATE]='MS' or [STATE]='MO' or [STATE]='MT' or [STATE]='NE' or [STATE]='NV' 
				 OR [STATE]='NH' or [STATE]='NJ' or [STATE]='NM' 
				 or [STATE]='NY' or [STATE]='NC' or [STATE]='ND' or [STATE]='OH' or [STATE]='OK' or [STATE]='OR' or [STATE]='PA' or [STATE]='RI' 
				 OR [STATE]='SC' or [STATE]='SD' or [STATE]='TN'
				 or [STATE]='TX' or [STATE]='UT' or [STATE]='VT' or [STATE]='VA' or [STATE]='WA' or [STATE]='WV' or [STATE]='WI' or [STATE]='WY')
				 group by [STATE] , [Country]   --'USA'
				 ORDER BY c desc
	
				alter table  safb_dq.dbo.[tblHeader]
				add stgCountry NVarchar(20)

				update safb_dq.dbo.[tblHeader]
				set stgCountry = 'United States'
				where([STATE] ='AL' or [STATE]='AK' or [STATE]='AZ' or [STATE]='AR'  or [STATE]='CA' or [STATE]='CO' or [STATE]='CT' or [STATE]='DE' 
				 OR [STATE]='DC' or [STATE]='FL'
				 or [STATE]='GA' or [STATE]='HI' or [STATE]='ID' or [STATE]='IL' or [STATE]='IN' or [STATE]='IA' or [STATE]='KS' or [STATE]='KY' 
				 OR [STATE]='LA' or [STATE]='ME' or [STATE]='MD'
				 or [STATE]='MA' or [STATE]='MI' or [STATE]='MN' or [STATE]='MS' or [STATE]='MO' or [STATE]='MT' or [STATE]='NE' or [STATE]='NV' 
				 OR [STATE]='NH' or [STATE]='NJ' or [STATE]='NM' 
				 or [STATE]='NY' or [STATE]='NC' or [STATE]='ND' or [STATE]='OH' or [STATE]='OK' or [STATE]='OR' or [STATE]='PA' or [STATE]='RI' 
				 OR [STATE]='SC' or [STATE]='SD' or [STATE]='TN'
				 or [STATE]='TX' or [STATE]='UT' or [STATE]='VT' or [STATE]='VA' or [STATE]='WA' or [STATE]='WV' or [STATE]='WI' or [STATE]='WY')
				 ---fnc 25018
	end


	begin --update country Extra_Address

				 SELECT [xa_state] , [xa_country], Count(*) c 
				 from  safb_dq.dbo.[tblExtraAddress] 
				 where 
				 ([xa_state] ='AL' or [xa_state]='AK' or [xa_state]='AZ' or [xa_state]='AR'  or [xa_state]='CA' or [xa_state]='CO' or [xa_state]='CT' or [xa_state]='DE' 
				 OR [xa_state]='DC' or [xa_state]='FL'
				 or [xa_state]='GA' or [xa_state]='HI' or [xa_state]='ID' or [xa_state]='IL' or [xa_state]='IN' or [xa_state]='IA' or [xa_state]='KS' or [xa_state]='KY' 
				 OR [xa_state]='LA' or [xa_state]='ME' or [xa_state]='MD'
				 or [xa_state]='MA' or [xa_state]='MI' or [xa_state]='MN' or [xa_state]='MS' or [xa_state]='MO' or [xa_state]='MT' or [xa_state]='NE' or [xa_state]='NV' 
				 OR [xa_state]='NH' or [xa_state]='NJ' or [xa_state]='NM' 
				 or [xa_state]='NY' or [xa_state]='NC' or [xa_state]='ND' or [xa_state]='OH' or [xa_state]='OK' or [xa_state]='OR' or [xa_state]='PA' or [xa_state]='RI' 
				 OR [xa_state]='SC' or [xa_state]='SD' or [xa_state]='TN'
				 or [xa_state]='TX' or [xa_state]='UT' or [xa_state]='VT' or [xa_state]='VA' or [xa_state]='WA' or [xa_state]='WV' or [xa_state]='WI' or [xa_state]='WY')
				 group by [xa_state] , [xa_Country]   --'USA'
				 ORDER BY c desc
	
				alter table  safb_dq.dbo.[tblExtraAddress]
				add stgXaCountry NVarchar(20)

				update safb_dq.dbo.[tblExtraAddress]
				set stgXaCountry = 'United States'
				 where ([xa_state] ='AL' or [xa_state]='AK' or [xa_state]='AZ' or [xa_state]='AR'  or [xa_state]='CA' or [xa_state]='CO' or [xa_state]='CT' or [xa_state]='DE' 
				 OR [xa_state]='DC' or [xa_state]='FL'
				 or [xa_state]='GA' or [xa_state]='HI' or [xa_state]='ID' or [xa_state]='IL' or [xa_state]='IN' or [xa_state]='IA' or [xa_state]='KS' or [xa_state]='KY' 
				 OR [xa_state]='LA' or [xa_state]='ME' or [xa_state]='MD'
				 or [xa_state]='MA' or [xa_state]='MI' or [xa_state]='MN' or [xa_state]='MS' or [xa_state]='MO' or [xa_state]='MT' or [xa_state]='NE' or [xa_state]='NV' 
				 OR [xa_state]='NH' or [xa_state]='NJ' or [xa_state]='NM' 
				 or [xa_state]='NY' or [xa_state]='NC' or [xa_state]='ND' or [xa_state]='OH' or [xa_state]='OK' or [xa_state]='OR' or [xa_state]='PA' or [xa_state]='RI' 
				 OR [xa_state]='SC' or [xa_state]='SD' or [xa_state]='TN'
				 or [xa_state]='TX' or [xa_state]='UT' or [xa_state]='VT' or [xa_state]='VA' or [xa_state]='WA' or [xa_state]='WV' or [xa_state]='WI' or [xa_state]='WY')
			
				 ---fnc 25018
	end

		
	begin --tbl header DQ Id
			alter table safb_dq.dbo.[tblHeader]
			add Legacy_Id__c NVarchar(20);
	  
			update safb_dq.dbo.[tblHeader] 
			set Legacy_ID__c = 'DQ-' + Cast(ID_Num as NVarchar(30))
		
			alter table safb_dq.dbo.[tblHeader]
			add Legacy_Id__2 NVarchar(20);
	 
			update safb_dq.dbo.[tblHeader] 
			set Legacy_Id__2 = 'DQ-' + Cast(ID_Num as NVarchar(30)) +'_2'
			where [tblHeader].[Secondary_Last_Name] is not null
	
			-- select id_num, legacy_id__c, Legacy_Id__2, firstname, lastname, Secondary_First_Name, Secondary_Last_Name from safb_dq.dbo.[tblHeader] as [th]
	
				
			select * from safb_dq.dbo.[tblHeader] as [th]
			where [th].[ID_Num] in ('68075','110806','1150995','1374744','1514449')

	end
		
	 begin  --tbl header: staging fields to use with charts:
			  --WON'T NEED these fields after SAFB cleans up DonorType in the table

			alter table [safb_dq].[dbo].[tblHeader]
				ADD [stgRecordTypeName] NVarchar(20)
			  , [stgAccountType] NVarchar(10)
		
			select distinct	th.[Donor_Type], th.[stgAccountType], th.[stgRecordTypeName]
						  , [cddt].donor_type, [cddt].RecordType, [cddt].AccountType
			from safb_dq.dbo.[tblHeader] as [th]
			left join safb_dq.dbo.[CHART_dq_donor_type] as [cddt] on th.[Donor_Type]= [cddt].Donor_Type  
			order by [cddt].[Donor_Type] 
		
			update safb_dq.dbo.[tblHeader]
			set [stgAccountType] = [cddt].AccountType, [stgRecordTypeName]=[cddt].[RecordType]
			from safb_dq.dbo.[tblHeader] as [th]
			left join [safb_dq].dbo.[CHART_dq_donor_type] as [cddt] on th.[Donor_Type] = [cddt].Donor_Type  

	end

	begin	--tblHeader address type
			
			alter table safb_dq.dbo.[tblHeader]
			add npsp__Address_Type__c NVarchar(20);
			 

			select distinct th.[Address_Type] , [adrt].code, [adrt].sf_fieldvalue, th.npsp__Address_Type__c
			from safb_dq.dbo.[tblHeader] as [th]
			inner join (select field_name, code, sf_FieldValue from [safb_dq].dbo.[CHART_dq_address_type] as [cdat] where [cdat].[field_name]='address_type') 
				as adrt on th.[Address_Type]=[adrt].[code]

			update safb_dq.dbo.[tblHeader]
			set npsp__Address_Type__c = adrt.sf_fieldValue
			from safb_dq.dbo.[tblHeader] as [th]
			inner join (select field_name, code, sf_FieldValue from [safb_dq].dbo.[CHART_dq_address_type] as [cdat] where [cdat].[field_name]='address_type') 
					as adrt on th.[Address_Type]=[adrt].[code]
			where adrt.[SF_FieldValue] is not null

	end
		
	begin--tblHeader notes
			--export NOTE_PAD Field to excel to produce
			--  text files for Note(ContentNote)

			--drop table
				drop table [safb_migration].dbo.stgTblNotes	;
 
			
			--create table
				select [th].[Legacy_Id__c], [th].[Note_Pad] as NoteBody
 				into [safb_migration].dbo.stgTblNotes
				from safb_dq.dbo.[tblHeader] as [th]
				where [th].[Note_Pad] is not null 
			  union all 
				select 'MEMO-'+Cast([tm].[Memo_Child_ID] as NVarchar(20)) legacy_id__c, [tm].[Memo_Text] as NoteBody
				--into [safb_migration].dbo.stgMemoNotes
				from safb_dq.dbo.[tblMemo] as [tm]
				inner join safb_dq.dbo.[tblHeader] as [th] on tm.[Memo_Parent_ID]=th.[ID_Num]		
				order by [Legacy_Id__c]

			
			--update HTML chars
		
				SELECT * from [safb_migration].dbo.stgTblNotes WHERE NoteBody like '%&%'
				SELECT * FROM [safb_migration].dbo.stgTblNotes where NoteBody like '%<%'
				SELECT * FROM [safb_migration].dbo.stgTblNotes WHERE NoteBody LIKE '%>%'
				SELECT * FROM [safb_migration].dbo.stgTblNotes WHERE NoteBody LIKE '%"%'
				SELECT * FROM [safb_migration].dbo.stgTblNotes WHERE NoteBody LIKE '%''%'

				UPDATE [safb_migration].dbo.[stgTblNotes] SET [NoteBody] =REPLACE([NoteBody] ,'&','&amp;') where [NoteBody]  LIKE '%&%'
				UPDATE [safb_migration].dbo.[stgTblNotes] SET [NoteBody] =REPLACE([NoteBody] ,'<','&lt;') where [NoteBody]  LIKE '%<%'
				UPDATE [safb_migration].dbo.[stgTblNotes] SET [NoteBody] =REPLACE([NoteBody] ,'>','&gt;') where [NoteBody]  LIKE '%>%'
				UPDATE [safb_migration].dbo.[stgTblNotes] SET [NoteBody] =REPLACE([NoteBody] ,'"','&quot;') where [NoteBody]  LIKE '%"%'
				UPDATE [safb_migration].dbo.[stgTblNotes] SET [NoteBody] =REPLACE([NoteBody] ,'''','&#39;') where [NoteBody]  LIKE '%''%'
		
			--RUN SELECT AND EXPORT TO EXCEL
			select * from [safb_migration].dbo.[stgTblNotes] as [sthnp] order by legacy_id__c

	end --86457 



	begin --donation fund and acccount.

		select distinct [td].[Donation_Fund] 
		from safb_dq.dbo.[tblDonation] as [td]
		where [td].[Donation_Fund] is null

		update [safb_dq].dbo.[tblDonation] set [tblDonation].[Donation_Fund]='NULL' where [tblDonation].[Donation_Fund] is null
		
		alter table [safb_dq].dbo.[tblDonation]
		add stgDonationAccount Varchar(10);
		
		update [safb_dq].dbo.[tblDonation] set stgDonationAccount = [tblDonation].[Donation_Account]  
		update [safb_dq].dbo.[tblDonation] set stgDonationAccount='NULL' where [tblDonation].stgDonationAccount is null

		select [td].[Donation_Fund], [td].[Donation_Account], stgDonationAccount , Count(*) c
		from safb_dq.dbo.[tblDonation] as [td]
		GROUP BY [td].[Donation_Fund], [td].[Donation_Account], stgDonationAccount 
		ORDER BY [td].[Donation_Fund], [td].[Donation_Account], stgDonationAccount 

	end

	begin --- stg field from charts

		update safb_dq.dbo.[tblDonation] set [tblDonation].[Donation_Type]='NULL' where [tblDonation].[Donation_Type] is null

		--test
			select [td].[Donation_Type], [td].[Donation_Fund], td.stgDonationAccount
 				,cdt.[npe01__Payment_Method__c], cdt.[Credit_Card_Type__c]
			from safb_dq.dbo.[tblDonation] as [td]
			left join [safb_dq].dbo.[CHART_DonationType] as [cdt] on [td].[Donation_Type] = [cdt].[Donation_Type]
			group BY [td].[Donation_Type], [td].[Donation_Fund], stgDonationAccount
			,cdt.[npe01__Payment_Method__c], cdt.[Credit_Card_Type__c]
			order BY [td].[Donation_Type], [td].[Donation_Fund], stgDonationAccount

	end 


begin---  
--1/ update tblDonation and add columnns from charts.
--2/ create multiple GAU records when 10K threshold is reached. 
	
	--add fields to tblDonation
			
			alter table [safb_dq].dbo.[tblDonation]
			add udfChildCampaign nvarchar(255),
				udfCampNo varchar(2), --for ntile group number
				udfGauNo varchar(2), 
				udfGAU_NAME nvarchar(100),
				GAU_Name nvarchar(100), 
				GAU_Desc nvarchar(255), 
				GAU_Active nvarchar(10),
				OPPORTUNITY_AccountCode nvarchar(255),
				ParentCampaign	nvarchar(255), 
				ParentCampaignDesc	nvarchar(255),
				ParentCampaignActive nvarchar(10), 
				ChildCampaign	nvarchar(255), 
				ChildCampaignDesc	nvarchar(255), 
				ChildCampaignActive nvarchar(10); 
	

	--update fund/acct field on table donation
		select distinct [td].[Donation_Fund], [td].[Donation_Account] , [td].[stgDonationAccount], td.[OPPORTUNITY_AccountCode]
		from [safb_dq].dbo.[tblDonation] as [td]
		where [td].[Donation_Fund] is null or [td].[Donation_Account] is null
		order by [td].[Donation_Fund], [td].[Donation_Account]
	
	
		update [safb_dq].dbo.[tblDonation]
		set  GAU_Name = t2.GAU_Name, GAU_Desc=t2.GAU_Description, GAU_Active=t2.GAU_Active,OPPORTUNITY_AccountCode= t2.[OPPORTUNITY_AccountCode]
		from [safb_dq].dbo.[tblDonation] as [t1]
		left join [safb_dq].dbo.CHART_DonationFundAccount as t2
		on t1.[Donation_Fund]=t2.[Donation_Fund] and  t1.[stgDonationAccount]=t2.[Donation_Account]

	--update campagin
		select * from safb_dq.dbo.[tblDonation] as [td] where [td].[Donation_Stimulus] is null or [td].[Donation_Appeal] is null;
		update safb_dq.dbo.[tblDonation] set [tblDonation].[Donation_Stimulus]='NULL' where [tblDonation].[Donation_Stimulus] is null;
		update safb_dq.dbo.[tblDonation] set [tblDonation].[Donation_Appeal]='NULL' where [tblDonation].[Donation_Appeal] is null;
				
		update [safb_dq].dbo.tblDonation
		set 	ParentCampaign	= t2.ParentCampaign	,
				ParentCampaignDesc	= t2.ParentCampaignDesc	,
				ParentCampaignActive = t2.ParentCampaignActive ,
				ChildCampaign =	t2.ChildCampaign,
				ChildCampaignDesc	=t2.ChildCampaignDesc	,
				ChildCampaignActive = t2.ChildCampaignActive 
		from [safb_dq].dbo.[tblDonation] as [t1]
		left join safb_dq.dbo.[CHART_DonationStimulusAppeal] as [t2] on t1.[Donation_Stimulus]= [t2].[Donation_Stimulus]
						and t1.[Donation_Appeal]= [t2].[Donation_Appeal]
				
			select distinct [td].[Donation_Stimulus], [td].[Donation_Appeal], td.ParentCampaign, td.ChildCampaign 
			from [safb_dq].dbo.[tblDonation] as [td]
			order by [td].[Donation_Stimulus], [td].[Donation_Appeal]



		--check for 10K threshold
		select --xt value for
		f.GAU_NAME 
		,count(*) Cnt					
		from safb_dq.dbo.[tblDonation] as t1
		inner join safb_dq.dbo.[tblHeader] as [th] on t1.Donation_Parent_ID=th.[ID_Num]
		left join safb_dq.dbo.[CHART_DonationFundAccount] as [f] on t1.[Donation_Fund]=f.[Donation_Fund] 
				and  Cast(t1.[Donation_Account] as Varchar(30))=f.[Donation_Account]
		where f.gau_name is not null 
		group by f.gau_name 
		having count(*) >5000
		order by cnt desc

			---GENERAL-OPS		223746	26 ntile() groups
			---THXGV-MEALS		22846	3  ntile() groups
			---HOLIDAY-MEALS	12483	2  ntile() groups
			---HARVEY			11961	2  ntile() groups

		
			--two groups
			with cte AS
		     (  	select --xt value for
					ntile(2) over (partition by [t1].gau_name order by [t1].gau_name) as ntileNo
					,t1.udfGauNo
					,t1.GAU_NAME--, count(*) CountGAU
					from safb_dq.dbo.[tblDonation] as t1
					inner join safb_dq.dbo.[tblHeader] as [th] on t1.Donation_Parent_ID=th.[ID_Num]
					where [t1].gau_name ='HARVEY' or [t1].gau_name='HOLIDAY-MEALS'
		     )
		     update cte
		     set udfGauNo = ntileNo
			--24444

			--three groups
			with cte AS
		     (  	select --xt value for
					ntile(3) over (partition by [t1].gau_name order by [t1].gau_name) as ntileNo
					,t1.udfGauNo
					,t1.GAU_NAME--, count(*) CountGAU
					from safb_dq.dbo.[tblDonation] as t1
					inner join safb_dq.dbo.[tblHeader] as [th] on t1.Donation_Parent_ID=th.[ID_Num]
					where [t1].gau_name ='THXGV-MEALS'
		     )
		     update cte
		     set udfGauNo = ntileNo
			 --22847
			
			 --thirty groups
			with cte AS
		     (  	select --xt value for
					ntile(28) over (partition by [t1].gau_name order by [t1].gau_name) as ntileNo
					,t1.udfGauNo
					,t1.GAU_NAME--, count(*) CountGAU
					from safb_dq.dbo.[tblDonation] as t1
					inner join safb_dq.dbo.[tblHeader] as [th] on t1.Donation_Parent_ID=th.[ID_Num]
					where [t1].gau_name ='GENERAL-OPS'
		     )
			  update cte
		     set udfGauNo = ntileNo
			 --223763
			 

			update [safb_dq].dbo.[tblDonation]
			set udfGAU_NAME = case when udfGauNo is not null then concat(GAU_NAME, '-',udfGauNo) else GAU_NAME end
		 	
			select --td.[Donation_Child_ID], 
			[td].[Donation_Fund], td.GAU_Name, udfGauNo, udfGAU_NAME, count(*) cnt
			from [safb_dq].dbo.[tblDonation] as [td]
			where [td].[Donation_Fund] like 'general%'
			group by --[td].[Donation_Child_ID],
			 [td].[Donation_Fund], td.GAU_Name, udfGauNo, udfGAU_NAME
			order by td.gau_name, cast(udfGauNo as int)
	
		 	
			select --td.[Donation_Child_ID], 
			td.GAU_Name, udfGauNo, udfGAU_NAME, count(*) cnt
			from [safb_dq].dbo.[tblDonation] as [td]
			where td.[udfGauNo] is not null
			group by --[td].[Donation_Child_ID],
			 td.GAU_Name, udfGauNo, udfGAU_NAME
			order by td.gau_name, cast(udfGauNo as int)
		

--check campaigns 
				 
	--check for 10K threshold
		select --xt value for
		t1.[ChildCampaign] 
		,count(*) Cnt					
		from safb_dq.dbo.[tblDonation] as t1
		inner join safb_dq.dbo.[tblHeader] as [th] on t1.Donation_Parent_ID=th.[ID_Num]
		where t1.[ChildCampaign]  is not null 
		group by t1.[ChildCampaign] 
		having count(*) >8000
		order by cnt desc

	/*	ChildCampaign	Cnt
		EMGVCAMP - AO-PRIVATE SUPP		37670
		WEBSITE - ONLINE				13067
		DM-INHOUSE - INDV-PHIL			11380
	*/

				
			with cte AS
		     (  	select --xt value for
					ntile(5) over (partition by [t1].[ChildCampaign] order by [t1].[ChildCampaign]) as ntileNo
					,t1.udfCampNo
					,t1.[ChildCampaign]--, count(*) Countchild
					from safb_dq.dbo.[tblDonation] as t1
					inner join safb_dq.dbo.[tblHeader] as [th] on t1.Donation_Parent_ID=th.[ID_Num]
					where t1.[ChildCampaign]='EMGVCAMP - AO-PRIVATE SUPP'
					 
		     )
		     update cte
		     set udfCampNo = ntileNo
			--37670

			with cte AS
		     (  	select --xt value for
					ntile(2) over (partition by [t1].[ChildCampaign] order by [t1].[ChildCampaign]) as ntileNo
					,t1.udfCampNo
					,t1.[ChildCampaign]--, count(*) Countchild
					from safb_dq.dbo.[tblDonation] as t1
					inner join safb_dq.dbo.[tblHeader] as [th] on t1.Donation_Parent_ID=th.[ID_Num]
					where t1.[ChildCampaign]='WEBSITE - ONLINE' or t1.[ChildCampaign]='DM-INHOUSE - INDV-PHIL'
					 
		     )
		     update cte
		     set udfCampNo = ntileNo
			 --24447

			update [safb_dq].dbo.[tblDonation]
			set udfChildCampaign = case when udfCampNo is not null then concat([ChildCampaign], '-',udfCampNo) else [ChildCampaign] end
			
			select --td.[Donation_Child_ID], 
			[td].[Donation_Stimulus], td.[Donation_Appeal], td.[ChildCampaign], udfCampNo, udfChildCampaign, count(*) cnt
			from [safb_dq].dbo.[tblDonation] as [td]
			where td.[ChildCampaign]='WEBSITE - ONLINE' or td.[ChildCampaign]='DM-INHOUSE - INDV-PHIL' or td.[ChildCampaign]='EMGVCAMP - AO-PRIVATE SUPP'
			group by --[td].[Donation_Child_ID],
			[td].[Donation_Stimulus], td.[Donation_Appeal], td.[ChildCampaign], udfCampNo, udfChildCampaign
			order by td.[ChildCampaign], udfCampNo
	
		 	
			select --td.[Donation_Child_ID], 
			td.[ChildCampaign], td.udfChildCampaign, count(*) cnt
			from [safb_dq].dbo.[tblDonation] as [td]
			where td.[udfCampNo] is not null
			group by --[td].[Donation_Child_ID],
			 td.[ChildCampaign], td.udfChildCampaign, td.udfcampno
			order by td.[ChildCampaign], td.udfChildCampaign, td.udfcampno


---Account soft credit
					drop table [safb_migration].dbo.stgAcctSoftCredit;

					select [t1].[Donation_Child_ID], th.[ID_Num], th.[OrgName] as Account_Soft_Credit__c
				 	into [safb_migration].dbo.stgAcctSoftCredit
					from [safb_dq].[dbo].[tblDonation] as [t1]
					join [safb_dq].[dbo].[tblHeader] as [th] on [t1].[Donation_Soft_Link]=[th].[ID_Num]
					join [safb_migration].[dbo].[xtr_account] as [xa] on [th].[Legacy_Id__c]=[xa].[Legacy_Id__c]
					join [safb_migration].[dbo].[xtr_opportunity] as [xt] on  'DQ-'+Cast([t1].[Donation_Child_ID]	as Varchar(30))=[xt].[legacy_id__c]
				    where [th].[stgRecordTypeName]='Organization'   ---OppContRole only to hh contacts. 
					order by t1.[Donation_Soft_Link], t1.[Donation_Child_ID]
					--56,406


					select ID_Num, Account_Soft_Credit__c, count(*) cnt 
					from [safb_migration].dbo.stgAcctSoftCredit
					group by Account_Soft_Credit__c, ID_Num
					order by Account_Soft_Credit__c 

