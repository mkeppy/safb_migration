USE [safb_dq]
GO

 
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_CLUB
	DROP TABLE [safb_migration].DBO.IMP_CLUB_MEMBER

END 

BEGIN -- CREATE IMP_CLUB
					select  distinct OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
  							,[Name] = cc.[RecordName]
							,Active__c = 'TRUE'   ---- Set to "TRUE" on all records
	 		 
			 		--into [safb_migration].DBO.IMP_CLUB
	 				from [safb_dq].dbo.[CHART_Club] as [cc] 
					where cc.[SF_Object]='CLUB'
				 
END -- TC1: 2

begin -- CREATE IMP_CLUB_MEMBER

					select  distinct OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
							,Account__c = xa.id  --lookup (account)
							,Contact__c = xc.id  --LOOKUP: Lookup (Contact)
							,Club__c = xcl.id	   -----LOOKUP: Lookup (Club)
							,Active__c = 'TRUE'   ---- Set to "TRUE" on all records
	 		 
							,t1.ID_Num	 as zrefIDNum
							,t1.[Club] as zrefClub

			 		into [safb_migration].DBO.IMP_CLUB_MEMBER
	 				from [safb_dq].dbo.[tblHeader] as t1
					inner join [safb_dq].dbo.[CHART_Club] as [cc] on [t1].[Club] = [cc].[Club]
					left join [safb_migration].dbo.xtr_account as xa on t1.[Legacy_Id__c]=xa.legacy_id__c
					left join [safb_migration].dbo.xtr_contact as xc on t1.[Legacy_Id__c]=xc.legacy_id__c
					inner join [safb_migration].dbo.xtr_club as xcl on cc.[RecordName] = xcl.[Name]
					where cc.[SF_Object]='CLUB'
					order by club__c, [Account__c], [Contact__c]
			 
END -- TC1: 498;  tc2: 568


BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_XXXXXXXXXXXX
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_XXXXXXXXXXXX GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_XXXXXXXXXXXX
	 

END 