use [safb_migration]
go


 
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_CAMPAIGN

END 

BEGIN -- CREATE IMP 

					select distinct   
						 OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 						,[Name] = Upper([cdsa].[ParentCampaign]) 
						,[Description] = [cdsa].[ParentCampaignDesc] 
						,[Parent:External_Id__c]  = null
						,[isActive] = [cdsa].[ParentCampaignActive] 
						,[Status] = case when [cdsa].[ParentCampaignActive]  = 'TRUE' then 'In Progress' else 'Completed' end
						,[External_Id__c] =  Upper([cdsa].[ParentCampaign])
						,zrefCampaignType ='1 parent'
			 		--into [safb_migration].DBO.IMP_CAMPAIGN
					from [safb_dq].dbo.[tblDonation]  as [cdsa]
					where [cdsa].[ParentCampaign] is not null 
				union 
					select distinct   
						 OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 						,[Name]  = upper([cdsa].[udfChildCampaign])   ---new camp name b/c of ntile()
						,[Description] = [cdsa].[ChildCampaignDesc] 
						,[Parent:External_Id__c] = Upper([cdsa].[ParentCampaign]) 
						,[isActive] = [cdsa].[ChildCampaignActive] 
						,[Status] = case when [cdsa].[ChildCampaignActive]  = 'TRUE' then 'In Progress' else 'Completed' end
						,[External_Id__c] =  upper([cdsa].[udfChildCampaign])    ---new camp name b/c of ntile()
						,zrefCampaignType ='2 child'
					from [safb_dq].dbo.tblDonation as [cdsa]
					where [cdsa].[ChildCampaign] is not null 
					order by zrefCampaignType , [Parent:External_Id__c], [Name], [Description]

				 
END -- TC1: 2340  ;  tc2. 2369

BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_CAMPAIGN
	WHERE [External_Id__c] in (SELECT [External_Id__c] from [safb_migration].DBO.IMP_CAMPAIGN group BY [External_Id__c] having COUNT(*)>1)
	ORDER BY [External_Id__c] 

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_CAMPAIGN
	 
	 select count(*) from [safb_dq].dbo.[tblDonation] as [td]

END 

