USE [safb_dq]
GO


--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
			0121I000000nGLJQA2	Account	Household Account
			0121I000000nGLKQA2	Account	Organization
	*/
 
 
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_VOL_VOLUNTEER_JOB

END 

BEGIN -- CREATE IMP 


					select   distinct
							 OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 							,[Legacy_Id__c] = 'CERVIS-'+ Cast(t2.camplegId as Varchar(20))    --- use the same campLegId as the legacy Id for vol jobs as these records are created from the same source, i.e. activity_name
							,Campaign__c = xc.id	-- Link to Campaign record
							,[Name] =min( t1.Activity_Name)
							,Primary_Interest_Category__c = min(replace(t1.Activity_Primary_Interest_Category,'&amp;','&'))-- Replace "&amp;" with "&"
							,Primary_Activity_Organizer__c = xu.id	 --LOOKUP: Lookup (User)
							,Location_Name__c = t1.Activity_Location	
							,Location_Street__c = stl.[Street]-- Parse address
							,Location_City__c = stl.city -- Parse address
/*API for location state*/  ,Location__c = stl.[State]-- Parse address
							,Location_Zip_Postal_Code__c = stl.[Zip] -- Parse address
							,Hours__c = null 
							,Description__c = null

							,'CERVIS-'+ Cast(t2.camplegId as Varchar(20)) as zrefCampLegId
			 				,zrefActivity_Id= t1.[Activity_ID]
					into [safb_migration].DBO.IMP_VOL_VOLUNTEER_JOB
			
					from [safb_dq].dbo.[tblVolCervis] as T1
					inner join [safb_migration].DBO.stgCervisCampId as t2 on t1.myid=t2.myid
					left join [safb_dq].dbo.stgActivityLocationAddress as stl on t1.[ActAddrSeq]=stl.[ActAddrSeq]
				    left join [safb_migration].dbo.xtr_campaign as xc on 'CERVIS-'+ Cast(t2.camplegId as Varchar(20))   = xc.External_Id__c
					left join (select * from [safb_dq].dbo.[CHART_Users] 
						       where [CHART_Users].[SourceField]='Primary_Activity_Organizer' and [CHART_Users].[Convert]='Yes') as cu
							   on  t1.Primary_Activity_Organizer=cu.[FieldValue]
				 	left join [safb_migration].dbo.[xtr_Users] as [xu] on cu.UserName = xu.[Username]
					group by  t2.camplegId , xc.id , xu.id,  t1.Activity_Location,
								 stl.[Street],  stl.[City],  stl.[State],  stl.[Zip], t1.[Activity_ID]
				union
					select   
							 OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
							,[Legacy_Id__c] = Concat('VOLSI-', Cast(t2.camplegId as Varchar(50)),' ',Upper(Left(t1.[Volunteer_Site_Location],2)))
							,Campaign__c = xc.id -- Link to Campaign record
							,[Name] = t1.Volunteer_Registration_Location	
							,Primary_Interest_Category__c = null 
							,Primary_Activity_Organizer__c = null
							,Location_Name__c = t1.Volunteer_Site_Location	
							,Location_Street__c = null
							,Location_City__c = null
/*API for location state*/  ,Location__c = null
							,Location_Zip_Postal_Code__c = null
 							,Hours__c = Sum(t1.[Hours])	-- Populate with the sum of hours per Job.
							,Description__c = Max(t1.Notes)
							,'VOLSI-'+ Cast(t2.camplegId as Varchar(20))   as zrefCampLegId
							,zrefActivity_Id=null
					from	[safb_dq].dbo.[tblVolWufoo]  as [t1]
					inner join (select distinct CampLegId, [Volunteer_Registration_Location]
								from [safb_migration].dbo.[stgVolSignInCampId] )as t2 on t1.[Volunteer_Registration_Location]=t2.[Volunteer_Registration_Location]
					inner join [safb_migration].dbo.xtr_campaign as xc on 'VOLSI-'+ Cast(t2.camplegId as Varchar(20)) = xc.[External_Id__c]
					group by t1.Volunteer_Registration_Location,  t1.Volunteer_Site_Location, t2.camplegId , xc.id

					

END -- TC1: 377;  tc2 418

BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_VOL_VOLUNTEER_JOB
--	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_VOL_VOLUNTEER_JOB group BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_VOL_VOLUNTEER_JOB
	 
	 select * from [safb_migration].dbo.IMP_VOL_VOLUNTEER_JOB

END 

begin--error

	select * from [safb_migration].dbo.imp_volunteer_job_xerror1

	alter table [safb_migration].dbo.imp_volunteer_job_xerror1
	drop column ErrorCode, ErrorMessage, ErrorColumn

	select distinct [Activity_Name], [Activity_ID], [Primary_Activity_Organizer] 
	from [dbo].[tblVolCervis] as [tvc] where activity_name ='Court Appointed May Volunteer Shift'