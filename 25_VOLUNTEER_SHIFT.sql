USE [safb_dq]
GO

--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
			0121I000000nGLJQA2	Account	Household Account
			0121I000000nGLKQA2	Account	Organization
	*/
  
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_VOL_VOLUNTEER_SHIFT

END 

BEGIN -- CREATE IMP 
				---TIME ZONE https://help.salesforce.com/articleView?id=supported_data_types.htm&type=5 

				select   distinct
						
							 Volunteer_Job__c = xvj.id	-- Link to Volunteer Job record --LOOKUP: Lookup (Volunteers__Volunteer_Job__C)
							,Duration__c = t1.[Duration]-- Required Field. Calculate value from Activity_Time
							,Start_Date_Time__c  = concat(convert(nvarchar(10), t1.[Activity_Date], 101) ,' ', t1.[StartTime])
						    ,Desired_Number_of_Volunteers__c = ev.Number_of_Volunteers_Needed
							,'CERVIS-'+ Cast(t2.camplegId as Varchar(20))  as zrefJobId
			 		
							,Legacy_Id__c = Concat('CERVIS-'+ Cast(t2.camplegId as Varchar(20))  ,'-',Convert(Varchar(10), t1.Activity_Date,126),'T',Convert(Varchar(12), Convert(Time, [t1].[StartTime])),'Z')	
							,zrefActDate = t1.[Activity_Date]
							,zrefStartTime = t1.[StartTime]
			
				--into [safb_migration].DBO.IMP_VOL_VOLUNTEER_SHIFT
			
					from [safb_dq].dbo.[tblVolCervis] as T1
					inner join [safb_migration].DBO.stgCervisCampId as t2 on t1.myid=t2.myid
					left join [safb_migration].dbo.xtr_Volunteer_Job as xvj on 'CERVIS-'+ Cast(t2.camplegId as Varchar(20))   = xvj.legacy_id__c
					left join [safb_dq].[dbo].[tblVolCervisTimeSlots] as ev on T1.[Activity_Name]=ev.event_name 
														and convert(nvarchar(10), T1.[Activity_Date], 101) = ev.EVDate
														and T1.[StartTime] = ev.EVTime

					where [T1].[Duration] is not null
					ORDER BY  xvj.id, [zrefActDate] desc
	 
END -- TC1: 8525; tc2 9680

BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_VOL_VOLUNTEER_SHIFT
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_VOL_VOLUNTEER_SHIFT group BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_VOL_VOLUNTEER_SHIFT
	
	select * from [safb_migration].dbo.IMP_VOL_VOLUNTEER_SHIFT
	where [IMP_VOL_VOLUNTEER_SHIFT].[zrefJobId]='CERVIS-309'
	order by [IMP_VOL_VOLUNTEER_SHIFT].[zrefJobId]

	select * from [dbo].[tblVolCervis] as [tvc] where [tvc].[Activity_ID]='47'

END 


		select [t1].[StartTime], Concat(convert(Varchar(10), t1.Activity_Date,126),' ',[t1].[StartTime])
		,Convert(DateTime,Concat(convert(Varchar(10), t1.Activity_Date,126),' ',[t1].[StartTime]),0) fmtsf
		,Concat(convert(Varchar(10), t1.Activity_Date,126),'T',Convert(Varchar(12), Convert(Time, [t1].[StartTime])),'Z') mydatetime
		from [dbo].[tblVolCervis] as [t1]



select * from [safb_migration].dbo.xerror_vol_shift


select * from [safb_migration].dbo.xtr_Volunteer_Job 

 