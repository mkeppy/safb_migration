use [safb_migration]
go
 
 select getdate(), convert(nvarchar(10),convert(time, getdate(), 100), 100)

  
--fb--0164

	select distinct [tvc].[Activity_ID], [tvc].[Activity_Name],
	[tvc].[Activity_Date],  [tvc].[StartTime],
	convert(nvarchar(10), [tvc].[Activity_Date], 101) as myDate
	from [safb_dq].dbo.[tblVolCervis] as [tvc]
	where [tvc].[Activity_Name]='SA Food Bank Warehouse Sorting & Packing April 2019'


	select event_name, event_slot_start_date
	,convert(nvarchar(10), Event_Slot_Start_Date, 101) as EVDate
	,replace(replace(convert(nvarchar(10), convert(time, Event_Slot_Start_Date, 100),100),'PM', ' PM'),'AM',' AM') as EVTime
 	from [safb_dq].[dbo].[tbl_SAFB_WP201904]
	   
	
---[tblVolCervisTimeSlots]	
	alter table [safb_dq].[dbo].[tblVolCervisTimeSlots]
	add EVDate nvarchar(10), EVTime nvarchar(10)

	update [safb_dq].[dbo].[tblVolCervisTimeSlots]
	set EVDate = convert(nvarchar(10), Event_Slot_Start_Date, 101),
	EVTime = replace(replace(convert(nvarchar(10), convert(time, Event_Slot_Start_Date, 100),100),'PM', ' PM'),'AM',' AM') 


	select distinct [tvc].[Activity_ID], [tvc].[Activity_Name],
	[tvc].[Activity_Date],  [tvc].[StartTime],
	ev.event_name, ev.EVDate, ev.EVTime, ev.Number_of_Volunteers_Needed as Desired_Number_of_Volunteers__c
	,convert(nvarchar(10), [tvc].[Activity_Date], 101) as myDate
	from [safb_dq].dbo.[tblVolCervis] as [tvc] 
	left join [safb_dq].[dbo].[tblVolCervisTimeSlots] as ev on [tvc].[Activity_Name]=ev.event_name 
														and convert(nvarchar(10), [tvc].[Activity_Date], 101) = ev.EVDate
														and [tvc].[StartTime] = ev.EVTime
	where [tvc].[Activity_Name]='SA Food Bank Warehouse Sorting & Packing April 2019'
	order by [tvc].[Activity_Name],	[tvc].[Activity_Date],  [tvc].[StartTime]


	select event_name, event_slot_start_date, EVDate, EVTime , Number_of_Volunteers_Needed as Desired_Number_of_Volunteers__c from [safb_dq].[dbo].[tbl_SAFB_WP201904]

	 
	 


--fb--0205

		select [th].[ID_Num], [th].[OrgName], [th].[FirstName], [th].[LastName], [th].[Donor_Type] 
		from [safb_dq].dbo.[tblHeader] as [th]
		where [th].[ID_Num]='7206' 

		select * from [safb_migration].dbo.[IMP_ACCOUNT] as [ia] where [ia].[Legacy_Id__c] like '%-7206%'
		
		select * from [safb_migration].dbo.imp_contact as [ia] where [ia].[Legacy_Id__c] like '%-7206%'

		select * from [safb_dq].dbo.[tblDonation] as [td]
		where [td].[Donation_Parent_ID]='7206'

		select contactid, count(*) Cnt
		from [safb_migration].dbo.[IMP_OPPORTUNITY_CONTACT_ROLE] as [iocr2] 
		group by [iocr2].[ContactId] , LEGACY_id__C
		having count(*)>1
		order by cnt desc

		select * from [safb_migration].dbo.[IMP_OPPORTUNITY_CONTACT_ROLE] as [iocr]
		where ContactId in (select contactid from [safb_migration].dbo.[IMP_OPPORTUNITY_CONTACT_ROLE] as [iocr2] group by [iocr2].[ContactId] having count(*)>1)
		order by [iocr].[ContactId], [iocr].[zrefDonId]

		select * from [safb_migration].dbo.[xtr_contact] as [xc] where [xc].[Id]='0030U00000dVxQGQA0'
		select * from [safb_migration].dbo.[xtr_account] as [xa] where [xa].[Legacy_Id__c]='7206'


		
					select distinct 
							 [OpportunityId] = xt.id					--LOOKUP: Lookup (Opportunity)
							,[ContactId] = xc.id						--LOOKUP: Lookup (Contact)
							,[Role] = 'Soft Credit' -- Role = "Soft Credit"
				--reference
							,t1.[Donation_Child_ID] as zrefDonId
							,t1.[Donation_Soft_Link] as zrefDonSoftLinkId
							,xc.[Legacy_Id__c] contactLegId
							,xt.[Legacy_Id__c] donationLegId

					from [safb_dq].[dbo].[tblDonation] as [T1]
					join [safb_dq].[dbo].[tblHeader] as [th] on [T1].[Donation_Soft_Link]=[th].[ID_Num]
					join [safb_migration].[dbo].[xtr_contact] as [xc] on [th].[Legacy_Id__c]=[xc].[Legacy_Id__c]
					join [safb_migration].dbo.xtr_opportunity as xt on  'DQ-'+Cast(t1.Donation_Child_ID	as Varchar(30))=xt.legacy_id__c
				    order by xc.id, xt.id

				select td.[Donation_Parent_ID], td.[Donation_Child_ID], [th].[Donor_Type], th.[OrgName], th.[FirstName], th.[LastName],
				[td].[Donation_Soft_Link], 
				[th1].[Donor_Type], th1.[OrgName], th1.[FirstName], th1.[LastName],
				count(*) Cnt
				from [safb_dq].dbo.[tblDonation] as [td] 
				left join [safb_dq].dbo.[tblHeader] as [th] on td.[Donation_Parent_ID]=th.[ID_Num]
				left join [safb_dq].dbo.[tblHeader] as [th1] on td.[Donation_Soft_Link]=th1.[ID_Num]
				where [td].[Donation_Soft_Link]='7206'
				group by td.[Donation_Parent_ID], [th].[Donor_Type], th.[OrgName], th.[FirstName], th.[LastName],
				[td].[Donation_Soft_Link],  td.[Donation_Child_ID],
				[th1].[Donor_Type], th1.[FirstName], th1.[LastName], th1.[OrgName]
				order by cnt desc






--fb-0215		
		select [th].[ID_Num], [th].[OrgName], [th].[LastName], [th].[Donor_Type] 
		from [safb_dq].dbo.[tblHeader] as [th]
		where [th].[ID_Num]='171764' or [th].[ID_Num]=' 171765'

		select * from [safb_migration].DBO.[xtr_contact] as [xc] where [xc].[Legacy_Id__c] like '%171764%' or [xc].[Legacy_Id__c] like '%171765%'

--fb-0160

		select xa.id, ia.[npo02__Informal_Greeting__c], ia.[npo02__Formal_Greeting__c]
		--into [safb_migration].dbo.imp_account_update
		from [safb_migration].dbo.[xtr_account] as [xa]
		inner join [safb_migration].dbo.[IMP_ACCOUNT] as [ia] on [xa].legacy_id__c =ia.[Legacy_Id__c]
		where ia.[npo02__Informal_Greeting__c] is not null or ia.[npo02__Formal_Greeting__c] is not null

		select * from [safb_dq].dbo.[tblHeader] as [th] where [th].[ID_Num]='80004'

--FB-0138

		select * 
		from [safb_dq].dbo.[tblVolCervis] as [tvc]
		
					select   distinct
							 OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 							,Volunteer_Job__c = xvj.id	-- Link to Volunteer Job record --LOOKUP: Lookup (Volunteers__Volunteer_Job__C)
							,Duration__c = t1.[Duration]-- Required Field. Calculate value from Activity_Time
							,Start_Date_Time__c  = Concat(convert(Varchar(10), t1.Activity_Date,126),'T',Convert(Varchar(12), Convert(Time, [t1].[StartTime])),'Z')	-- Required Field. Concatenate with start time of Activity_Time
						    ,t1.[Activity_Date]
							,t1.[StartTime]
							,CreatedDate = Concat(convert(Varchar(10), t1.Activity_Date,126),'T',Convert(Varchar(12), Convert(Time, [t1].[StartTime])),'Z')	-- Required Field. Concatenate with start time of Activity_Time
							,'CERVIS-'+ Cast(t2.camplegId as Varchar(20))  as zrefJobId
				 		
							,Legacy_Id__c = Concat('CERVIS-'+ Cast(t2.camplegId as Varchar(20))  ,'-',Convert(Varchar(10), t1.Activity_Date,126),'T',Convert(Varchar(12), Convert(Time, [t1].[StartTime])),'Z')	

					into [safb_migration].dbo.ztest_vol_shift_timezone  -- drop table  [safb_migration].dbo.ztest_vol_shift_timezone
					from [safb_dq].dbo.[tblVolCervis] as T1
					inner join [safb_migration].DBO.stgCervisCampId as t2 on t1.myid=t2.myid
					left join [safb_migration].dbo.xtr_Volunteer_Job as xvj on 'CERVIS-'+ Cast(t2.camplegId as Varchar(20))   = xvj.legacy_id__c
					where [T1].[Duration] is not null
					and xvj.[Legacy_Id__c]='CERVIS-342' 

					and t1.[Activity_Date]='2018-05-01 00:00:00.000'
					order by [T1].[Activity_Date], [T1].[StartTime]
					
					select * from [safb_migration].dbo.ztest_vol_shift_timezone_upd
					truncate table [safb_migration].dbo.ztest_vol_shift_timezone_upd

						
--FB-0139

		select * from [safb_dq].[dbo].[tblVolCervis] as [tvc]
		where [tvc].[CERVIS_ID]='10759'
		order by [tvc].[Activity_ID]

		--created vol hours sql file  to update vol hr records.







--FB-->??

		select distinct [tvc].[Activity_ID], [tvc].[Activity_Name]
		from [safb_dq].[dbo].[tblVolCervis] as [tvc]
		order by [tvc].[Activity_ID]  --362
		
		
		select distinct [tvc].[Activity_ID]
		from [safb_dq].[dbo].[tblVolCervis] as [tvc]
		order by [tvc].[Activity_ID]  --323
		
		select * from [safb_migration].dbo.[stgCervisCampId]  





		 select  th.id_num, th.salutation1 , th.salutation2 , th.salutation3 
		 from [dbo].[tblHeader] as [th] 
		 where [th].[ID_Num]='10631' or [th].[ID_Num]='173253'





--FB---065
		select [th].[Do_Not_Solicit], Count(*) cr
		 from [dbo].[tblHeader] as [th]
		 group by [th].[Do_Not_Solicit]

		 select [th].[Inactive], Count(*) cr
		 from [dbo].[tblHeader] as [th]
		 group by [th].[Inactive]

		 select [th].[Do_Not_Acknowledge], Count(*) cr
		 from [dbo].[tblHeader] as [th]
		 group by [th].[Do_Not_Acknowledge]


		  select [th].[Bad_Address], Count(*) cr
		 from [dbo].[tblHeader] as [th]
		 group by [th].[Bad_Address]




		 select [th].[ID_Num], [th].[Do_Not_Solicit],[th].[Inactive], [th].[Do_Not_Acknowledge] , [th].[Bad_Address]
		 from [dbo].[tblHeader] as [th] where [th].[ID_Num]='4759' or [th].[ID_Num]='126920'





--------------
	select * from [safb_dq].dbo.[tblHeader] as [th] where [th].[ID_Num]='64' or [th].[ID_Num]='7206' or [th].[ID_Num]='82' or [th].[ID_Num]='29315' or [th].[ID_Num]='18142'

	select count(*) from [safb_dq].dbo.[tblHeader] as [th]

	select count(*) from [safb_dq].dbo.[tblHeader] as [th]
	select count(*) from [safb_dq].dbo.[tblExtraAddress] as [tea]

	select distinct id_num from [safb_dq].dbo.[tblHeader] as [th]

--FB-0008 -- affiliation

			select * from [safb_migration].dbo.[IMP_AFFILIATION] as [ia]

--FB-0007

			select * from [safb_migration].DBO.IMP_RELATIONSHIP 

			select * from [safb_migration].DBO.IMP_RELATIONSHIP 
			where [IMP_RELATIONSHIP].[npe4__Contact__c]='0030U00000XQr7TQAT' or [IMP_RELATIONSHIP].[npe4__RelatedContact__c]='0030U00000XQr7TQAT'

			select * from [safb_dq].dbo.[tblLinkage] as [tl]  
			 





--FB-0010 -- Opportunity Campaign
				select * from [safb_migration].DBO.IMP_OPPORTUNITY 
 				where legacy_Id__c ='DQ-306542'

				select [td].[Donation_Child_ID], [td].[Donation_Amount], [td].[Donation_Date]
						,[td].[Donation_Appeal], [td].[Donation_Stimulus], [td].[Donation_Fund] 
						from [safb_dq].dbo.[tblDonation] as [td] where [td].[Donation_Child_ID]='306542'
		
				select * from [safb_dq].dbo.[CHART_DonationStimulusAppeal] as [cdsa]
				where [cdsa].[Donation_Appeal]='WEBSITE' and [cdsa].[Donation_Stimulus]='Nov-15'



				select t.* , [cdsa].*
				from [safb_dq].dbo.tblAppealStim as t
				left join [safb_dq].dbo.[CHART_DonationStimulusAppeal] as [cdsa] on t.donation_stimulus=cdsa.[Donation_Stimulus]
																				and t.donation_appeal= [cdsa].[Donation_Appeal]
				where [cdsa].[Donation_Stimulus] is null or [cdsa].[Donation_Appeal] is null
				order by [cdsa].[Donation_Appeal]

				select * from [safb_dq].dbo.[CHART_DonationStimulusAppeal] as [cdsa] 
				where [cdsa].[Donation_Stimulus] like '4%'

				update [safb_dq].dbo.[CHART_DonationStimulusAppeal] 
				set [CHART_DonationStimulusAppeal].[Donation_Stimulus]='Nov-15' 
				where [CHART_DonationStimulusAppeal].[Donation_Stimulus]='42309'
				
				update [safb_dq].dbo.[CHART_DonationStimulusAppeal] 
				set [CHART_DonationStimulusAppeal].[Donation_Stimulus]='Dec-15' 
				where [CHART_DonationStimulusAppeal].[Donation_Stimulus]='42339'

				select * from [safb_dq].dbo.[CHART_DonationStimulusAppeal] as [cdsa] 
				where [cdsa].[Donation_Stimulus] like 'Nov-%' or [cdsa].[Donation_Stimulus] like 'Dec-%'


				select xo.id, [io].[CampaignId], xo.[Legacy_Id__c] as refLegId, [io].[Legacy_Appeal__c] refLegApp, [io].[Legacy_Stimulus__c] refLegStim
				from [safb_migration].dbo.[IMP_OPPORTUNITY] as [io]
				left join [safb_migration].dbo.[xtr_Opportunity] as [xo] on io.[Legacy_Id__c]=xo.[Legacy_Id__c]
				where [io].[Legacy_Stimulus__c] like 'Nov-%' or [io].[Legacy_Stimulus__c] like 'Dec-%'
				 

--FB-0011 --Users
		select distinct xu.id,  xu.[Username], xp.[Name] as ProfileName, cu.[TimeZoneSidKey], xu.[ProfileId], xp.id pid
		from [safb_migration].dbo.[xtr_Users] as [xu]
		inner join [safb_dq].dbo.[CHART_Users] as [cu] on xu.[Username]=cu.[UserName]
		left join [safb_migration].dbo.[xtr_Profiles] as [xp] on xu.[ProfileId]=xp.id
		 

		 select * from [dbo].[xtr_Users] as [xu]
		 select * from [dbo].[IMP_USERS] as [iu]

--FB-0006  --Contact record type

		select xc.[Id], ic.[RecordTypeId] 
		into [safb_migration].dbo.zimp_contact_update
		from [safb_migration].dbo.[IMP_CONTACT] as [ic]
		join [safb_migration].dbo.[xtr_contact] as [xc] on ic.[Legacy_Id__c]=xc.[Legacy_Id__c]


--FB 0004  - Account.Active__c

		select xa.id, ia.[Active__c] 
		into [safb_migration].dbo.zimp_account_update
		from [safb_migration].dbo.[IMP_ACCOUNT] as [ia]
		join [safb_migration].dbo.[xtr_account] as [xa] on ia.[Legacy_Id__c]=xa.[Legacy_Id__c]


		select * from [safb_migration].dbo.zimp_account_update where [zimp_account_update].[Active__c] ='false'

		select * from [safb_dq].[dbo].[tblHeader] as [th] where [th].[Inactive]='y'

--FB-00005 --Address

		SELECT * FROM [safb_migration].DBO.IMP_ADDRESS
		where [IMP_ADDRESS].[Legacy_Id__c] is null

