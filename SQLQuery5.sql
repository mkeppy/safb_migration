use [dq]
go

select distinct [Activity_Name] , activity_date
from [dbo].[tblVolCervis]
order by [Activity_Name], activity_date


select distinct primary_activity_organizer , Count(*)
from [dbo].[tblVolCervis]
group by primary_activity_organizer


select distinct [Activity_Location_Address] 
from [dbo].[tblVolCervis]
order by [Activity_Location_Address] 