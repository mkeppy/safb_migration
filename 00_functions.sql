



  
  use [safb_migration]
  go 

-- FUNCTION: Set OwnerID
 	 
			--select * from dbo.xtr_users

			create  function [dbo].[fnc_OwnerId]()         
		  
			RETURNS   varchar(18)  
			BEGIN 
				declare @Owner as varchar(18)     
				set @Owner = '0050U000001910FQAQ' --Deja
				return @Owner 
            end 

-- STORED PROCEDURE FOR PHONE NUMBERS STORED IN CONSTITUENT ADDRESSES.
		create PROCEDURE HC_PivotWizard_p
		   @P_Row_Field    VARCHAR(4000),
		   @P_Column_Field VARCHAR(4000),
		   @P_Value        VARCHAR(4000),
		   @P_Into         VARCHAR(4000),
		   @P_From         VARCHAR(4000),
		   @P_Where        VARCHAR(4000) = '1=1'

		AS

		  DECLARE @SQL NVARCHAR(4000)

		  -- Build SQL statment that upload @Columns string 
		  -- with @P_Column_Filed values
		  CREATE TABLE #TEMP  (ColumnField varchar(255))
		  SET @sql ='SELECT DISTINCT '+@P_Column_Field+' AS ColumnField'+
					  ' FROM '+@P_From+
					  ' WHERE '+@P_Where+
					  ' ORDER BY '+@P_Column_Field
		  INSERT INTO #TEMP
		  EXEC(@sql)
		  --PRINT @sql

		  -- Check count of columns
		  DECLARE @Count_Columns int
		  SELECT @Count_Columns = COUNT(*) FROM #Temp
		  IF (@Count_Columns<1) OR (@Count_Columns>255)  BEGIN
			  DROP TABLE #Temp
			  RAISERROR('%d is invalid columns amount. Valid is 1-255',
						16,1,@Count_columns)
			  RETURN
		  END
		  -- Upload @Columns from #Temp
		  DECLARE @Columns VARCHAR(max),
				  @Column_Field VARCHAR(max)

		  SET @Columns = ''
		  DECLARE Column_cursor CURSOR LOCAL FOR
		  SELECT CAST(ColumnField AS VARCHAR(60))
		  FROM #Temp
		  OPEN Column_cursor
		  FETCH NEXT FROM Column_cursor
		  INTO @Column_Field 
		  WHILE @@FETCH_STATUS = 0 BEGIN
			  SET @Columns = @Columns +
				' MAX('+
					 ' CASE WHEN '+@P_Column_Field+'='''+ @Column_Field+''''+
					 ' THEN '+@P_Value+
					 ' ELSE null END'+
					 ') AS ['+ @Column_Field +'], '
			  FETCH NEXT FROM Column_cursor
			  INTO @Column_Field
		  END
		  CLOSE Column_cursor
		  DEALLOCATE Column_cursor
		  DROP TABLE #Temp

		  IF @Columns='' RETURN 1
		  SET @Columns = Left(@Columns,Len(@Columns)-1)

		  -- Build Pivot SQL statment
		  DECLARE @Pivot_SQL VARCHAR(max)
		  SET @Pivot_SQL =              'SELECT '  +@P_Row_Field+', '+@Columns
		  SET @Pivot_SQL = @Pivot_SQL +' INTO '    +@P_Into  
		  SET @Pivot_SQL = @Pivot_SQL +' FROM '    +@P_From
		  SET @Pivot_SQL = @Pivot_SQL +' WHERE '   +@P_Where
		  SET @Pivot_SQL = @Pivot_SQL +' GROUP BY '+@P_Row_Field
		  SET @Pivot_SQL = @Pivot_SQL +' ORDER BY '+@P_Row_Field
		  SET @Pivot_SQL = @Pivot_SQL + '#'

		  IF Right(@Pivot_SQL,1)<>'#'
		  BEGIN
			 RAISERROR('SQL statement is too long. It must be less
						than 8000 charachter!',16,1)
			 RETURN 1
		  END
		  SET @Pivot_SQL = Left(@Pivot_SQL,Len(@Pivot_SQL)-1)

		  -- PRINT @Pivot_SQL
		  EXEC(@Pivot_SQL)

		  RETURN 0
		go  
        

---PROPER

	create FUNCTION Proper(@Text AS VARCHAR(8000) )
		RETURNS VARCHAR(8000)
		AS
			BEGIN
				DECLARE @Reset BIT;
				DECLARE @Ret VARCHAR(8000);
				DECLARE @i INT;
				DECLARE @c CHAR(1);

				SELECT  @Reset = 1 ,
						@i = 1 ,
						@Ret = '';

				WHILE ( @i <= LEN(@Text) )
					SELECT  @c = SUBSTRING(@Text, @i, 1) ,
							@Ret = @Ret + CASE WHEN @Reset = 1 THEN UPPER(@c)
											   ELSE LOWER(@c)
										  END ,
							@Reset = CASE WHEN @c LIKE '[a-zA-Z]' THEN 0
										  ELSE 1
									 END ,
							@i = @i + 1
				RETURN @Ret
			END


			
CREATE FUNCTION udfFormatUnformattedPhone
	(@cPhone	CHAR(10))
	RETURNS		CHAR(14)
AS
BEGIN
	RETURN '(' + SUBSTRING(@cPhone, 1, 3) + ') ' + 
               SUBSTRING(@cPhone, 4, 3) + '-' + 
               SUBSTRING(@cPhone, 7, 4) 
END


 
CREATE FUNCTION fnStandardPhone (@PhoneNumber VARCHAR(32))
RETURNS VARCHAR(32)
AS
  BEGIN
    DECLARE  @Phone CHAR(32)
    

    SET @Phone = @PhoneNumber
    
    -- cleanse phone number string
    WHILE PATINDEX('%[^0-9]%',@PhoneNumber) > 0
      SET @PhoneNumber = REPLACE(@PhoneNumber,
               SUBSTRING(@PhoneNumber,PATINDEX('%[^0-9]%',@PhoneNumber),1),'')
    
    -- skip foreign phones
    IF (SUBSTRING(@PhoneNumber,1,1) = '1'
         OR SUBSTRING(@PhoneNumber,1,1) = '+'
         OR SUBSTRING(@PhoneNumber,1,1) = '0')
       AND LEN(@PhoneNumber) > 11
      RETURN @Phone
    
    -- build US standard phone number
    SET @Phone = @PhoneNumber
    
    SET @PhoneNumber = '(' + SUBSTRING(@PhoneNumber,1,3) + ') ' +
             SUBSTRING(@PhoneNumber,4,3) + '-' + SUBSTRING(@PhoneNumber,7,4)
    
    IF LEN(@Phone) - 10 > 1
      SET @PhoneNumber = @PhoneNumber + ' X' + SUBSTRING(@Phone,11,LEN(@Phone) - 10)
    
    RETURN @PhoneNumber
  END
GO