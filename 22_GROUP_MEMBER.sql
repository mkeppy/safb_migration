USE [safb_dq]
GO

--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
			0121I000000nGLJQA2	Account	Household Account
			0121I000000nGLKQA2	Account	Organization
	*/
 
 
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_VOL_GROUP_MEMBER

END 
 
 begin	
			select distinct OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()    
					,Group_Member__c = xc.id
					,Group__c = xg.id

					,sgl.Legacy_Id__c as zrefGroupLeadCervisID
					,tvgl.[Group_ID] as zrefGroupID
				    ,[tvgl].[Pri_Group_Contact_Name] as zrefContName
				    ,tvgl.[Pri_Group_Contact_CERVIS_ID] zrefCervisPriGroCntId
			
		into [safb_migration].dbo.IMP_VOL_GROUP_MEMBER		
			from [safb_dq].dbo.[tblVolGroupLeader] as [tvgl]
			 join [safb_migration].dbo.stgGroupLeaderContacts as sgl on [tvgl].[Pri_Group_Contact_CERVIS_ID]=sgl.[Pri_Group_Contact_CERVIS_ID]
			 join [safb_migration].dbo.xtr_contact as xc on sgl.legacy_id__c = xc.legacy_Id__c
			 join [safb_migration].dbo.xtr_group as xg on Cast([tvgl].[Group_ID] as Varchar(20)) = xg.legacy_Id__c
		union all 
			select distinct OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()    
					,Group_Member__c = xc.id
					,Group__c = xg.id

					,sgl.Legacy_Id__c as zrefGroupLeadCervisID
					,tvgl.[Group_ID] as zrefGroupID
				    ,[tvgl].[Pri_Group_Contact_Name] as zrefContName
				    ,tvgl.[Pri_Group_Contact_CERVIS_ID] as zrefCervisPriGroCntId
					
			from [safb_dq].dbo.[tblVolGroupLeader] as [tvgl]
			left join [safb_migration].dbo.stgGroupLeaderContacts as sgl on [tvgl].[Pri_Group_Contact_CERVIS_ID]=sgl.[Pri_Group_Contact_CERVIS_ID]
			inner join [safb_migration].dbo.xtr_contact as xc on 'CERVIS-'+Cast(tvgl.[Pri_Group_Contact_CERVIS_ID] as Varchar(20)) = xc.legacy_Id__c   --link only contacts created from the Cervis table. 
			inner join [safb_migration].dbo.xtr_group as xg on [tvgl].[Group_ID] = xg.legacy_Id__c
			 
end
			 
			 select * from [safb_migration].dbo.[IMP_VOL_GROUP_MEMBER] as [ivgm] order by group__c

			 select   [Group_ID], count(*) c from [safb_dq].dbo.[tblVolGroupLeader]  as [sglc]
			 join [safb_dq].dbo.[tblVolCervis] as [tvc] on [sglc].[Group_ID]= [tvc].[CERVIS_ID]
			 group by [Group_ID]
			 order by c desc