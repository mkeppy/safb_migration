


--unique code values
		select [Marital_Status], Count(*) as cnt
		from [safb_dq].dbo.[Header]
		group by [Marital_Status]

		select [Address_Type], Count(*) as cnt
		from [safb_dq].dbo.[Header]
		group by [Address_Type]

		select [Donor_Type], Count(*) as cnt
		from [safb_dq].dbo.[Header]
		group by [Donor_Type]


		select [Code], Count(*) as cnt
		from [safb_dq].dbo.[Header]
		group by [Code]

		select [Rank], Count(*) as cnt
		from [safb_dq].dbo.[Header]
		group by [Rank]

		select [Club], Count(*) as cnt
		from [safb_dq].dbo.[Header]
		group by [Club]

		select [Division], Count(*) as cnt
		from [safb_dq].dbo.[Header]
		group by [Division]


		select [Solicitor], Count(*) as cnt
		from [safb_dq].dbo.[Header]
		group by [Solicitor]

		select Lists.code_description,  Sum([count]) as total
		from(	select distinct [List1] as [List] , count (*) as [count] from [safb_dq].dbo.[Header] group by List1
				union
				select distinct [List2] as [List] , count (*) as [count] from [safb_dq].dbo.[Header] group by List2
				union
				select distinct [List3] as [List] , count (*) as [count] from [safb_dq].dbo.[Header] group by List3
				union
				select distinct [List4] as [List] , count (*) as [count] from [safb_dq].dbo.[Header] group by List4
				union
				select distinct [List5] as [List] , count (*) as [count] from [safb_dq].dbo.[Header] group by List5
				union
				select distinct [List6] as [List] , count (*) as [count] from [safb_dq].dbo.[Header] group by List6
				union
				select distinct [List7] as [List] , count (*) as [count] from [safb_dq].dbo.[Header] group by List7
				union
				select distinct [List8] as [List] , count (*) as [count] from [safb_dq].dbo.[Header] group by List8
				union
				select distinct [List9] as [List] , count (*) as [count] from [safb_dq].dbo.[Header] group by List9
				union
				select distinct [List10] as [List] , count (*) as [count] from [safb_dq].dbo.[Header] group by List10
				
		) as t
		left join [safb_dq].dbo.list1 s on [t].[List]=s.code
		group by t.list , s.code_description
		order by t.[List]


		select [Profile] , Sum([Count]) Total
		from
		(select distinct [Profile1] as [Profile]	, Count(*) [Count] from [safb_dq].dbo.[Header] group by [Profile1]
		union
		select distinct [Profile2] as [Profile] , Count(*) [Count] from [safb_dq].dbo.[Header] group by [Profile2]
		union
		select distinct [Profile3] as [Profile] , Count(*) [Count] from [safb_dq].dbo.[Header] group by [Profile3]
		union
		select distinct [Profile4] as [Profile] , Count(*) [Count] from [safb_dq].dbo.[Header] group by [Profile4]
		union
		select distinct [Profile5] as [Profile] , Count(*) [Count] from [safb_dq].dbo.[Header] group by [Profile5]
		) as t
		group by [Profile] 
		order by [t].[Profile]

--donor types

	select [ID_Num], [Donor_Type], [OrgName], [FirstName], [LastName], [Secondary_First_Name], [Secondary_Last_Name] , [Other_Name]
	from [safb_dq].dbo.[tblHeader]
	order by [Donor_Type], [OrgName], [LastName], [Secondary_Last_Name]


	select [ID_Num], [Donor_Type], [OrgName], [FirstName], [LastName], [Secondary_First_Name], [Secondary_Last_Name] , [Other_Name]
	from [safb_dq].dbo.[tblHeader]
	where [Donor_Type]='indv' and [OrgName] is not null
	order by [Donor_Type], [OrgName], [LastName], [Secondary_Last_Name]


		select t.[Donor_Type], t.[FirstName], t.[LastName], t.[OrgName], Sum(t.rc) as rct
 		from (select h.[Donor_Type]
				,case when h.[FirstName] is not null then 'TRUE' else null end as FirstName
 				,case when h.[LastName] is not null then 'TRUE' else null end as LastName
				,case when h.[OrgName] is not null then 'TRUE' else null end as OrgName
				, Count(*) as rc
				from [safb_dq].dbo.[tblHeader] h
				group by [h].[Donor_Type], h.[LastName], h.[OrgName], h.[FirstName]
			  ) as t
		group by t.[Donor_Type], t.[LastName], t.[OrgName], t.[FirstName]
		order by t.[Donor_Type], t.[LastName], t.[OrgName];


		select t.[ID_Num], t.[Donor_Type], t.[FirstName], t.[LastName], t.[OrgName] ,t.[When_Donor_Added], t.[Last_Date_Edited], t.[Who_Last_Edited], t.[Who_Added_Donor] 
		from [safb_dq].dbo.[tblHeader] t
		where ((t.[FirstName] is not null and t.[LastName] is not null and t.[OrgName] is not null) 
					and (t.[Donor_Type]='corp' or t.[Donor_Type]='org' or t.[Donor_Type]='indv' or t.[Donor_Type]='found' or t.[Donor_Type]='prospect' or t.[Donor_Type]='fb'))
			or ((t.[Donor_Type] is null or t.[Donor_Type] ='indv') and (t.[LastName] is null and t.[OrgName] is null and t.[FirstName] is null))
			or ((t.[Donor_Type] ='indv') and (t.[LastName] is null and t.[OrgName] is null and t.[FirstName] is not null))
			or ((t.[Donor_Type] ='corp') and (t.[LastName] is null and t.[OrgName] is not null and t.[FirstName] is not null))
			or ((t.[Donor_Type] ='corp' or t.[Donor_Type] ='indv' or t.[Donor_Type] ='org') and (t.[LastName] is not null and t.[OrgName] is not null and t.[FirstName] is  null))
		
		order by t.[Donor_Type], t.[LastName], t.[OrgName];





		select [ID_Num], [Donor_Type], [FirstName], [LastName], [OrgName] 
		from [safb_dq].dbo.[tblHeader]
		order by [Donor_Type], [LastName], [OrgName]

		select * from [safb_dq].dbo.[tblHeader] 
		where [Donor_Type] is null
		and [LastName] is null
		and [FirstName] is null
		and [OrgName] is null



--phones
		select [ID_Num], [Donor_Type], [OrgName], [LastName], [Telephone], [Work_Phone], [Fax_Phone], [Mobile_Phone], [EMail], [Web_Address]
		from [safb_dq].dbo.[Header] 
		order by [Donor_Type], [OrgName], [LastName]

--ID_NUM len
		select [ID_Num], Len([ID_Num]) ln 
		from [safb_dq].dbo.[Header]
		order by ln desc

 --relationship
	select Relationship_Between_Primary_And_Secondary, Count(*) as cnt 
	from [safb_dq].dbo.[tblHeader]  
	group by Relationship_Between_Primary_And_Secondary

--contact
	select SC_Action, Count(*) as Count
	from [dbo].[Contacts]
	group by [SC_Action]
	order by [SC_Action]

 	select [SC_Status], Count(*) as Count
	from [dbo].[Contacts]
	group by [SC_Status]
	order by [SC_Status]

	select [SC_Number], Count(*) as Count
	from [dbo].[Contacts]
	group by [SC_Number]
	order by [SC_Number]

	select c.[SC_Number], c.[ID_Num], c.[SC_Parent_ID], c.[SC_Child_ID] 
 	,p.[LastName], p.[FirstName], p.[OrgName]
 	,case when c.[ID_Num]<>c.[SC_Parent_ID] then 'false' else 'true' end as id_equal_parent, c.*
	from [safb_dq].dbo.[Contacts] as c
 	left join [safb_dq].dbo.[Header] as p on c.[SC_Parent_ID]=p.[ID_Num]
 	order by id_equal_parent asc , c.[ID_Num], [c].[SC_Number]


	--xtra address

		select c.[XA_Number], c.[ID_Num], c.[XA_Parent_ID], c.[XA_Child_ID] 
	 	,concat(p.[FirstName],' ',p.[LastName]) as ParentName, p.[OrgName] as ParentOrgName, p.[Address], p.[Secondary_First_Name], p.[Secondary_Last_Name]
	 	,case when c.[ID_Num]<>c.[XA_Parent_ID] then 'false' else 'true' end as id_equal_parent, c.*
		from [safb_dq].[dbo].[Extra_Addresses] c
	 	left join [safb_dq].dbo.[Header] as p on c.[XA_Parent_ID]=p.[ID_Num]
	 	order by id_equal_parent asc , c.[ID_Num], [c].[XA_Number]
		
			select [XA_Code], Count(*) as Count
			from [dbo].[Extra_Addresses]
			group by [XA_Code]
			order by [XA_Code]


	--linkages
		select * from [safb_dq].dbo.[tblLinkage]

		select l.[Linkage_Child_ID], l.[ID_Num], l.[Linkage_Number], l.[Linkage_Parent_ID], l.[Linkage_Linked_To]
	 	,concat(p.[FirstName],' ',p.[LastName]) as ParentName, p.[OrgName] as ParentOrgName  
	 	,concat(lt.[FirstName],' ',lt.[LastName]) as LinkedtoName, lt.[OrgName] as LinkedToOrgName  
	 
		from [safb_dq].[dbo].[tblLinkage]as l
	 	left join [safb_dq].dbo.[tblHeader] as p on l.[Linkage_Parent_ID]=p.[ID_Num]
		left join [safb_dq].dbo.[tblHeader] as lt on l.[Linkage_Linked_To]=lt.[ID_Num]
		order by l.[ID_Num], l.[Linkage_Number] asc


		
		select p.[Donor_Type] as ParentIdDonorType, lt.[Donor_Type] as LinkedToDonorType, Count(*) RecCnt	 
		from [safb_dq].[dbo].[tblLinkage] as l
	 	left join [safb_dq].dbo.[tblHeader] as p on l.[Linkage_Parent_ID]=p.[ID_Num]
		left join [safb_dq].dbo.[tblHeader] as lt on l.[Linkage_Linked_To]=lt.[ID_Num]
		group by p.[Donor_Type], lt.[Donor_Type]
		order by p.[Donor_Type], lt.[Donor_Type]
		 
		select * from [safb_dq].[dbo].[tblLinkage] 


--volunteer sign in

		select * from [safb_dq].dbo.[cervis_volunteers_signin]
		where [Entry_Id] in (select [Entry_Id] from [safb_dq].dbo.[cervis_volunteers_signin] group by [Entry_Id] having Count(*)>1)
		order by [Entry_Id]

		select distinct * 
		into [safb_dq].dbo.tbl_volunteers_signin
		from [safb_dq].dbo.[cervis_volunteers_signin]

-- addres type

	select * from [safb_dq].dbo.address_type

--contact
	select * from SC_Action

--donor type

	select * from [safb_dq].dbo.donor_type

--profile
	select * from [safb_dq].dbo.profile1

--lists
	select * from [safb_dq].dbo.list1

--relationship

	select * from [safb_dq].dbo.[Relation_bt_Primary&Secondary] order by [Code]

--code
	select * from [safb_dq].dbo.club
	select * from [safb_dq].dbo.marital_Status
	select * from [safb_dq].dbo.xa_code


---division
	select h.division, d.code_description, Count(*) [count] 
	from [safb_dq].dbo.[Header] as h
	left join [safb_dq].dbo.division d on h.[Division]=d.code
	group by h.[Division], d.code_description
	order by h.[Division]


---division
	select h.solicitor, d.code_description, Count(*) [count] 
	from [safb_dq].dbo.[tblHeader] as h
	left join [safb_dq].dbo.solicitor d on h.[solicitor]=d.code
	group by h.[solicitor], d.code_description
	order by h.[solicitor]


--pledge 
	select * from [safb_dq].dbo.DonationPledge_type
	select * from [safb_dq].dbo.DonationPledge_account


	select 
		p.[Pledge_Account],	a.code_Description,
		p.[Pledge_Stimulus],s.code_Description,
		p.[Pledge_Fund],	f.code_Description,
		p.[Pledge_Appeal],	l.code_Description,
		Count(*) [Count]
	from [safb_dq].dbo.[Pledges] p 
	left join [safb_dq].dbo.donationpledge_account a on p.[Pledge_Account] = a.code
	left join [safb_dq].dbo.donationpledge_stimulus s on p.[Pledge_Stimulus] = s.code
	left join [safb_dq].dbo.donationpledge_fund f on p.[Pledge_Fund] = f.code
	left join [safb_dq].dbo.donationpledge_appeal l on p.[Pledge_Appeal] = l.code	
	group by 
		p.[Pledge_Account],	a.code_Description,
		p.[Pledge_Stimulus],s.code_Description,
		p.[Pledge_Fund],	f.code_Description,
		p.[Pledge_Appeal],	l.code_Description

	order by p.[Pledge_Account], p.[Pledge_Stimulus], p.[Pledge_Fund], p.[Pledge_Appeal]


--ID NUM discrepancy.

	select t.[ID_Num], t.[Donation_Parent_ID], t.[Donation_Child_ID] 
	,h.[FirstName], h.[LastName], h.[OrgName] 
	,p.[FirstName] parentFirstName, p.[LastName] parentLastName, p.[OrgName] parentOrgName
	,t.*
	from [safb_dq].dbo.[tblDonation] t
	left join [safb_dq].dbo.[tblHeader] h on [t].[ID_Num] = [h].[ID_Num]
	left join [safb_dq].dbo.[tblHeader] p on [t].[Donation_Parent_ID]= [p].[ID_Num] 
	where t.[ID_Num]<>t.[Donation_Parent_ID]
	--29 records
	 
	  
	select t.[ID], t.[Memo_Parent_ID], t.[Memo_Child_ID]
	,h.[FirstName], h.[LastName], h.[OrgName] 
	,p.[FirstName] parentFirstName, p.[LastName] parentLastName, p.[OrgName] parentOrgName
	,t.*
	from [safb_dq].dbo.[tblMemo] t
	left join [safb_dq].dbo.[tblHeader] h on [t].id = [h].[ID_Num]
	left join [safb_dq].dbo.[tblHeader] p on [t].[Memo_Child_ID]= [p].[ID_Num] 
	where t.[ID]<>t.[Memo_Parent_ID]
	--6 records

	select t.[ID_Num], t.[XA_Parent_ID], t.[XA_Child_ID]
	,h.[FirstName], h.[LastName], h.[OrgName] 
	,p.[FirstName] parentFirstName, p.[LastName] parentLastName, p.[OrgName] parentOrgName
	,t.*
	from [safb_dq].dbo.[tblExtraAddress] t
	left join [safb_dq].dbo.[tblHeader] h on [t].[ID_Num] = [h].[ID_Num]
	left join [safb_dq].dbo.[tblHeader] p on [t].[XA_Child_ID]= [p].[ID_Num] 
	where t.[ID_Num]<>t.[XA_Parent_ID]
	--6 records


	select [Group_Membership], Count(*) 
	from [dbo].[tblVolCervis]

	group by [Group_Membership]


	
	select [What_is_the_name_of_the_volunteer_group_you_are_with?], Count(*) 
	from [dbo].[tblVolSignIn_duped]

	group by [What_is_the_name_of_the_volunteer_group_you_are_with?]


	
	select [Shift], Count(*) 
	from [dbo].[tblVolSignIn_duped]

	group by [Shift]


	
	
	select [Volunteer_Registration_Location], Count(*) c
	from [dbo].[tblVolSignIn_duped]

	group by [Volunteer_Registration_Location]
	order by c desc

	select [Activity_Location_Address], Count(*) c
	from [dbo].[tblVolCervis]
	group by [Activity_Location_Address]
	order by [Activity_Location_Address]

	select [Activity_Location], Count(*) c
	from [dbo].[tblVolCervis]
	group by [Activity_Location]
	order by [Activity_Location]


	select distinct [Terms & Conditions] from [dbo].[tblVolCervis]


	select distinct registration_source from [dbo].[tblVolCervis]
	
	select activity_id, Count(*) 
	from 	(select distinct Activity_Id, Activity_Name 
			from [dbo].[tblVolCervis]) tbl
	group by activity_id
	having Count(*)>1