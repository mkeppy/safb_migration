
--update greeting fields

	select xa.id, ia.[npo02__Informal_Greeting__c], ia.[npo02__Formal_Greeting__c]
		into [safb_migration].dbo.imp_account_update_greeting
		from [safb_migration].dbo.[xtr_account] as [xa]
		inner join [safb_migration].dbo.[IMP_ACCOUNT] as [ia] on [xa].legacy_id__c =ia.[Legacy_Id__c]
		where ia.[npo02__Informal_Greeting__c] is not null or ia.[npo02__Formal_Greeting__c] is not null
