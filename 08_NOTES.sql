USE [safb_dq]
GO

 
BEGIN -- DROP IMP
	DROP TABLE [safb_migration].DBO.IMP_CONTENT_NOTE
END 

BEGIN -- CREATE IMP 
					select   OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 							,Title = Concat('DQ Memo Date Added: ', (case when t1.[Memo_When_Added] is null  then Cast(t1.[Memo_When_Last_Edited] as Date)  else Cast( t1.Memo_When_Added as Date)	 end)   , '; ', mc.[CODE_DESCRIPTION] )-- Use Description in memo_code table and concatenate with Memo_Child_Id	-- Yes/Link (dbo.memo_code.code)
							,Content = 'D:\DATA_MIGRATIONS\SAFB\Import_Files\Notes_ContentNote\MEMO-'+Cast(t1.Memo_Child_ID as NVarchar(20))  + '.txt' 	-- Path = path to folder with files to be uploaded.	
							,CreatedDate = case when t1.[Memo_When_Added] is null  then t1.[Memo_When_Last_Edited]  else t1.Memo_When_Added	 end
							,LinkedEntityId = xa.id--LOOKUP: Lookup	
							,ShareType = 'V'	-- Which type to use:   "V" Viewer permission. The user can explicitly view but not edit the shared file.  "C" Collaborator permission. The user can explicitly view and edit the shared file.  "I" Inferred permission. The user�s permission is determined by th	
  		
						 
							,zrefAcctLegacyId = [th].[Legacy_Id__c]
							,zrefChildId = t1.[Memo_Child_ID]
							,zrefSource = 'tblMemo'
					 
		  	 		into [safb_migration].DBO.IMP_CONTENT_NOTE
 					from [safb_dq].dbo.[tblMemo] as T1
					join [safb_dq].dbo.[tblHeader] as [th] on t1.[Memo_Parent_ID]=th.[ID_Num]
					join [safb_migration].dbo.xtr_account as xa on th.[Legacy_Id__c]=xa.legacy_id__c
					left join [safb_dq].dbo.[Memo_Code] as [mc] on t1.[Memo_Code]=mc.[CODE]	
				union 
					select   OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 							,Title = Concat('DQ Donor Notes. ', Cast(th.[When_Donor_Added] as Date)) 
							,Content = 'D:\DATA_MIGRATIONS\SAFB\Import_Files\Notes_ContentNote\'+th.[Legacy_Id__c] + '.txt' 	-- Path = path to folder with files to be uploaded.	
							,CreatedDate = th.[When_Donor_Added] 
							,LinkedEntityId = xa.id--LOOKUP: Lookup	
							,ShareType = 'V'	-- Which type to use:   "V" Viewer permission. The user can explicitly view but not edit the shared file.  "C" Collaborator permission. The user can explicitly view and edit the shared file.  "I" Inferred permission. The user�s permission is determined by th	
  			 
							,zrefAcctLegacyId = [th].[Legacy_Id__c]
							,zrefChildId = th.[ID_Num]
							,zrefSource = 'tblHeader'
			
					from [safb_dq].dbo.[tblHeader] as [th]  
					join [safb_migration].dbo.xtr_account as xa on th.[Legacy_Id__c]=xa.legacy_id__c
					where th.[Note_Pad] is not null
					order by zrefSource, [zrefChildId]

				 
END -- TC1: 86457

--requires two uploads
-- 1. ContentNote object 
-- 2. ContentDocumentLink


BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_CONTENT_NOTE
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_XXXXXXXXXXXX GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_XXXXXXXXXXXX
	
	select count(*) from [safb_dq].dbo.[tblMemo] as [tm]


	SELECT * FROM [safb_migration].DBO.IMP_CONTENT_NOTE
	order by [IMP_CONTENT_NOTE].[LinkedEntityId]



END 