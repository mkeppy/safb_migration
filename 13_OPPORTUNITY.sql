USE [safb_dq]
GO

--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
				SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
	*/
  
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_OPPORTUNITY

END 

BEGIN -- CREATE IMP 
					select   OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
							,[Name]  =  concat(xa.[Name],' $', ltrim(str(t1.[Donation_Amount], 25, 2)), ' Donation ', convert(nvarchar(10), t1.[Donation_Date], 101)) 
							,CloseDate = case when t1.Donation_Date is not null then t1.[Donation_Date]
											  when t1.[Donation_When_Added] is not null then t1.[Donation_When_Added]
											  when t1.[Donation_When_Last_Edited] is not null then t1.[Donation_When_Last_Edited] 
											  else null  end
							,Amount = t1.Donation_Amount	
							,Account_Code__c = [t1].[OPPORTUNITY_AccountCode]	

							,CampaignId = iif(xc.id is null, xcp.id, xc.id)  --- if child campaign in chart is null, then parent campaign in chart.
 
 							,Legacy_Appeal__c = case when t1.Donation_Appeal ='NULL' then null else t1.[Donation_Appeal] end
							,Legacy_Stimulus__c = case when t1.Donation_Stimulus ='NULL' then null else t1.Donation_Stimulus end
							,Legacy_Fund__c = case when t1.Donation_Fund='NULL' then null else t1.Donation_Fund end
							,Acknowledgment_Letter__c = dtc.[CODE_DESCRIPTION]
							,Acknowledgment_Amount__c = t1.[Donation_TY_Value]
							,[Description] = t1.Donation_Comment	
							,CreatedDate = case when t1.Donation_When_Added	is null 
												then t1.[Donation_When_Last_Edited] 
												else t1.[Donation_When_Added]  end
							,AccountId = xa.id	 --LOOKUP: Lookup (Account)
							,Legacy_Id__c = 'DQ-'+cast(t1.Donation_Child_ID	as varchar(30))
							,RecordTypeId = '0121I000000nGLLQA2'	-- Record type = "Donation" --LOOKUP: Lookup (Recordtype)			 
			 				,StageName = 'Closed Won'
							,[asc].[Account_Soft_Credit__c]

							--dnc fb-0165-,Board_Gift__c = iif(th.[Club]='BOARD LEAD','true', 'false')

							,th.[ID_Num] as zrefDonorId

							,zrefCampParExtId= [t1].[ParentCampaign]
							,zrefCampChiExtId=[t1].[udfChildCampaign]
                            ,zrefSeq = row_number() over (order by  xa.id, xc.id)
					--into [safb_migration].DBO.IMP_OPPORTUNITY
			
					from [safb_dq].dbo.[tblDonation] as t1
					inner join [safb_dq].dbo.[tblHeader] as [th] on t1.Donation_Parent_ID=th.[ID_Num]
					inner join [safb_migration].dbo.xtr_account as xa on th.[Legacy_Id__c]=xa.legacy_id__c
					left join [safb_dq].dbo.[Donation_TY_Code] as [dtc] on [t1].[Donation_TY_Code]=[dtc].[CODE]
		 			left join [safb_migration].[dbo].[xtr_campaign] as [xc] on [t1].[udfChildCampaign]=[xc].[external_id__c]   ---  ---new camp name b/c of ntile()
					left join [safb_migration].[dbo].[xtr_campaign] as [xcp] on [t1].[ParentCampaign]=[xcp].[external_id__c]  
 					left join [safb_migration].[dbo].[stgAcctSoftCredit] as [asc] on [t1].[Donation_Child_ID]=[asc].[Donation_Child_ID]
				 
END -- TC1: 319434  tc2: 454,778



BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_OPPORTUNITY where [IMP_OPPORTUNITY].[Board_Gift__c] ='true'
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_OPPORTUNITY group BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_OPPORTUNITY

	--missing data	 
	 select * from [safb_migration].dbo.[IMP_OPPORTUNITY] as [io]
	 where [io].[Name] is null or [io].[RecordTypeId] is null or [io].[StageName] is null
	 or [io].[CloseDate]  is null or [io].[Amount] is null or [io].[AccountId] is null



	SELECT td.[Donation_Parent_ID], td.[Donation_Child_ID] , td.[Donation_Amount], [op].[zrefDonorId], [op].[Legacy_Id__c]
	,th.[ID_Num], th.[FirstName], th.[LastName], th.[OrgName]
	from [safb_dq].dbo.[tblDonation] as [td] 
	 join [safb_dq].dbo.[tblHeader] as [th] on [td].[Donation_Parent_ID]= [th].[ID_Num]
	left join [safb_migration].DBO.IMP_OPPORTUNITY as op on op.[Legacy_Id__c]='DQ-'+Cast(td.Donation_Child_ID	as Varchar(30))
	where op.[Legacy_Id__c] is null and td.donation_Amount <0

	
	select * from [safb_migration].dbo.[xtr_Opportunity] as [xo]
	where amount <0
	 
END 

begin--EXCEPTIONS

	select  *
	from [safb_migration].dbo.imp_opportunity_xerror6
	order by ErrorMessage

	update [safb_migration].dbo.imp_opportunity_xerror3
	set Name = replace(Name, 'Members Give / JustGive.Org (formerly - Giving Express Online From American Express Powered By JustG','Members Give / JustGive.Org')
	where errormessage like '%name%'

	update [safb_migration].dbo.imp_opportunity_xerror3
	set Description = replace(Description, 'Members Give / JustGive.Org (formerly - Giving Express Online From American Express Powered By JustG','Members Give / JustGive.Org')
	where errormessage like '%name%'

	alter table [safb_migration].dbo.imp_opportunity_xerror5
	drop column ErrorCode, ErrorColumn, ErrorMessage

	'Nora Sanchez Advanced Screening of The Hunger Games: Mockingjay - Part I - Nora Snchez'



	select [Legacy_Id__c], count(*) C 
	from [safb_migration].dbo.xtr_opportunity 
	group by legacy_Id__c
	order by c desc
end


truncate table [safb_migration].dbo.xtr_opportunity


select * from [safb_migration].dbo.xtr_opportunity



