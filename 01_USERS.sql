use [safb_migration]
go


begin--review
	select * from [safb_dq].dbo.[CHART_Users] as [cu]
	select * from [safb_migration].dbo.[xtr_Users] as [xu]
	 

end


begin -- users 

						select      distinct
									[OwnerId] = [safb_migration].[dbo].[fnc_OwnerId]()
								   ,[xu].[Id]
								   ,[IsActive] = iif([cu].[IsActive]=1, 'true','false')
								   ,[cu].[UserName]
								   ,[cu].[Email]
								   ,[cu].[FirstName]
								   ,[cu].[LastName]
								   ,[cu].[Title]
								   ,[cu].[Alias]
								   ,[cu].[CommunityNickname]
								   ,[xp].[Id] as [ProfileId]
								   ,[cu].[LanguageLocaleKey]
								   ,[cu].[TimeZoneSidKey]
								   ,[EmailEncodingKey] = 'ISO-8859-1'
								   ,[LocaleSidKey] = 'en_US'
						into [safb_migration].dbo.IMP_USERS_update
						from        [safb_dq].[dbo].[CHART_Users] as [cu]
						  left join [safb_migration].[dbo].[xtr_Profiles] as [xp] on [cu].[ProfileName] = [xp].[Name]
						  left join [safb_migration].dbo.[xtr_Users] as [xu] on [cu].[UserName] = [xu].[Username]
						where       [cu].[Convert] = 'yes'
						order by    [cu].[LastName]
								   ,[cu].[FirstName];


end;

select * from [safb_migration].dbo.IMP_USERS

select * from [safb_migration].dbo.xtr_users

truncate table [safb_migration].dbo.[xtr_Users]