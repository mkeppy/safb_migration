use [safb_migration]
go

begin --review

	select * from [safb_dq].dbo.[CHART_DonationFundAccount] as [cdfa] 
	where [cdfa].[Convert]='Yes' order by [cdfa].[GAU_Name] desc 

end

BEGIN -- DROP IMP
	DROP TABLE [safb_migration].DBO.IMP_GAU
END 

BEGIN -- CREATE IMP 

	
					select   distinct 
							OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 			 				,t1.[udfGAU_Name] as [Name]
							,'TRUE' as npsp__Active__c    --must first be true for upload of allocations
							,t1.[GAU_Desc] as [npsp__Description__c]
							,npsp__Active__c_update = max(t1.[GAU_Active])
 			 		into [safb_migration].DBO.IMP_GAU
			
					from safb_dq.dbo.tblDonation as t1
					where t1.[udfGAU_Name] is not null		
					group by t1.[udfGAU_NAME], t1.[GAU_Desc]
					order by [t1].[udfGAU_Name]		 
			 

				 
END -- TC1: 881  --991

BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_GAU
	WHERE [Name] in (SELECT [Name]  from [safb_migration].DBO.IMP_GAU group BY [Name]  having COUNT(*)>1)
	ORDER BY [Name]  

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_GAU
	 

END 
