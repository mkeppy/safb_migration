use [safb_migration]
go

--update HomePhone
		select	id
				, [xc].[Legacy_Id__c] as refLegId
				, [xc].[npe01__PreferredPhone__c] as refPrefPhone
				, [xc].[HomePhone] as oldHomePhone, [xc].[Phone] as oldPhone
				, clnPhone = Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[HomePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','') 
				, clnPhoneLen = Len(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[HomePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) 
 				, HomePhone = dbo.udfFormatUnformattedPhone(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[HomePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) 
				, Phone = dbo.udfFormatUnformattedPhone(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[HomePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) 
		
		into dbo.zUpdate_HomePhone_clean
		from [dbo].[xtr_contact] as [xc]
		where [xc].[HomePhone] not like '%x%' and [xc].[HomePhone] is not null
		and Len(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[HomePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) =10
		order by xc.[AccountId], xc.id


		select *
		from [safb_migration].dbo.zUpdate_HomePhone_clean where id ='0030U00000XQgWFQA1'


--update WorkPhone
		select	id
				, [xc].[Legacy_Id__c] as refLegId
				, [xc].[npe01__PreferredPhone__c] as refPrefPhone
				, [xc].[npe01__WorkPhone__c] as oldWorkPhone
				, xc.[HomePhone] as oldHomePhone
				, [xc].[Phone]as oldPhone
				, clnPhone = Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[npe01__WorkPhone__c], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','') 
				, clnPhoneLen = Len(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[npe01__WorkPhone__c], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) 
 				, [npe01__WorkPhone__c] = dbo.udfFormatUnformattedPhone(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[npe01__WorkPhone__c], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) 
				, HomePhone = dbo.udfFormatUnformattedPhone(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[HomePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) 
				, Phone = case when [npe01__PreferredPhone__c]='Work' then dbo.udfFormatUnformattedPhone(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[npe01__WorkPhone__c], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) 
							   when [npe01__PreferredPhone__c]='Home' then dbo.udfFormatUnformattedPhone(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[HomePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) 
							   else null 
						  end
		into dbo.zUpdate_WorkPhone_clean
		from [dbo].[xtr_contact] as [xc]
		where [xc].[npe01__WorkPhone__c] not like '%x%' and [xc].[npe01__WorkPhone__c]is not null
		and Len(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[npe01__WorkPhone__c], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) =10
		order by xc.[npe01__PreferredPhone__c], xc.[AccountId], xc.id


		select *
		from [safb_migration].dbo.zUpdate_WorkPhone_clean



--update MobilePhone
		select	id
				, [xc].[Legacy_Id__c] as refLegId
				, [xc].[npe01__PreferredPhone__c] as refPrefPhone
				, [xc].[MobilePhone] as oldMobilePhone
				, xc.[HomePhone] as oldHomePhone
				, [xc].[Phone]as oldPhone
				, clnPhone = Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[MobilePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','') 
				, clnPhoneLen = Len(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[MobilePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) 
 				, MobilePhone = dbo.udfFormatUnformattedPhone(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[MobilePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) 
				, HomePhone = dbo.udfFormatUnformattedPhone(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[HomePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) 
				, Phone = case when [npe01__PreferredPhone__c]='Mobile' then dbo.udfFormatUnformattedPhone(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[MobilePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) 
							   when [npe01__PreferredPhone__c]='Home' then dbo.udfFormatUnformattedPhone(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[HomePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) 
							   else null 
						  end
		--into dbo.zUpdate_WorkPhone_clean
		from [dbo].[xtr_contact] as [xc]
		where [xc].[MobilePhone] not like '%x%' and ([xc].[MobilePhone] is not null or [xc].[npe01__PreferredPhone__c]='Mobile')
		and Len(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(xc.[MobilePhone], '-',''), '(',''),')',''),' ',''),'.',''),'/',''),'=',''),':','')) =10
		order by xc.[npe01__PreferredPhone__c], xc.[AccountId], xc.id

--update OtherP

