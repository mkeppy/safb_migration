USE [safb_dq]
GO

 
 
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_BOARD_COMMITTEE
	DROP TABLE [safb_migration].DBO.IMP_BOARD_COMMITTEE_MEMBER

END 

BEGIN -- CREATE IMP_BOARD_COMMITTEE
					select  distinct OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
  							,[Name] = cc.[RecordName]
							,Active__c = 'TRUE'   ---- Set to "TRUE" on all records
	 		 
			 		into [safb_migration].DBO.IMP_BOARD_COMMITTEE
	 				from [safb_dq].dbo.[CHART_Club] as [cc] 
					where cc.[SF_Object]='BOARD/COMMITTEE'
				   
				   select * from [safb_migration].DBO.IMP_BOARD_COMMITTEE
END -- TC1: 3

begin -- CREATE IMP_BOARD_COMMITTEE_MEMBER

					select  distinct  
							Contact__c = xc.id --LOOKUP: Lookup (Contact)
							,Board_Committee__c = xbc.id	   -----LOOKUP: Lookup (Board_Committee)
							,Active__c = 'TRUE'   ---- Set to "TRUE" on all records
	 		 
							,t1.ID_Num	 as zrefIDNum
							,t1.[Club] as zrefClub

			 		into [safb_migration].DBO.IMP_BOARD_COMMITTEE_MEMBER
	 				from [safb_dq].dbo.[tblHeader] as t1
					inner join [safb_dq].dbo.[CHART_Club] as [cc] on [t1].[Club] = [cc].[Club]
					inner join [safb_migration].dbo.xtr_contact as xc on t1.[Legacy_Id__c]=xc.legacy_id__c
					inner join [safb_migration].dbo.xtr_board_committee as xbc on cc.[RecordName] = xbc.[Name]
					where cc.[SF_Object]='BOARD/COMMITTEE'
					order by Board_Committee__c, contact__c

END -- TC1: 46;  tc2: 61


BEGIN -- AUDIT
	 

	SELECT * FROM [safb_migration].DBO.IMP_XXXXXXXXXXXX
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_XXXXXXXXXXXX GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_XXXXXXXXXXXX
	 

END 