USE [safb_dq]
GO

--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
			0121I000000nGLJQA2	Account	Household Account
			0121I000000nGLKQA2	Account	Organization
	*/
 
 
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_VOL_GROUP

END 

BEGIN -- CREATE IMP 

					select OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 			 
		 					,Legacy_Id__c = t1.Group_ID	-- Migrate only groups that match with the CERVIS_ID in the tblCervis.
							,[Name] = t1.Group_Name	
							--,Type__c = t1.Group_Type	
							,Comments__c = t1.Group_Notes	
							,Sum(cast(tvc.[Activity_Hours] as float)) as Legacy_Group_Hours__c
			
							,zrefCervisId=tvc.[CERVIS_ID] 
							,zrefGroupId=t1.[Group_ID]
					
				--	into [safb_migration].DBO.IMP_VOL_GROUP
			
					from [safb_dq].dbo.[tblVolGroupLeader] as t1
					join [safb_dq].dbo.[tblVolCervis] as [tvc] on t1.[Group_ID]=tvc.[CERVIS_ID] -- Migrate only groups that match with the CERVIS_ID in the tblCervis.
					GROUP BY t1.[Group_ID], t1.[Group_Name], 
					 --t1.[Group_Type],
					 t1.[Group_Notes]
					 ,tvc.[CERVIS_ID]
					
				
END -- TC1: 1781

BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_VOL_GROUP
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_VOL_GROUP group BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_VOL_GROUP
	 

END 