USE [safb_dq]
GO

BEGIN --Generate scripts from map.
			select  [OrderNo], [Source_Field],  [Convert], SF_Object, SF_Field_API, 
					[SCRIPT] = ','+SF_Field_API+' = t1.'+ [Source_Field],
				 	[TRANSLATIONRULES] = CASE WHEN [Translation_Rules] IS NULL THEN '' ELSE '-- '+[Translation_Rules]  END +
										 case when [Data_Type] like '%lookup%' then ' --LOOKUP: ' +[Data_Type] else '' end,
					[CONVERTLINKREF] = case WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END  
			FROM [safb_dq].dbo.DATA_TABLE 
			WHERE  SF_Object LIKE '%XXXXXXXXXXXX%'  --OR  [Translation_Rules] IS NOT NULL 
			order by Cast([OrderNo] as Int)

			select  [OrderNo], [Source_Field],  [Convert], SF_Object_2, SF_Field_API_2, 
					[SCRIPT] = ','+SF_Field_API_2+' = t1.'+ [Source_Field],
				 	[TRANSLATIONRULES] = CASE WHEN [Translation_Rules_2] IS NULL THEN '' ELSE '-- '+[Translation_Rules_2]  END +
										 case when [Data_Type_2] like '%lookup%' then ' --LOOKUP: ' +[Data_Type_2] else '' end, 
					[CONVERTLINKREF] = case WHEN LEN([Convert])>3 THEN '-- '+[Convert] ELSE '' END  
			FROM [safb_dq].dbo.DATA_TABLE as [h] 
			WHERE  SF_Object_2 LIKE '%XXXXXXXXXXXX%' --OR  [Translation_Rules_2] IS NOT NULL
			order by Cast([OrderNo] as Int) 
END 

--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
			0121I000000nGLJQA2	Account	Household Account
			0121I000000nGLKQA2	Account	Organization
	*/
 
 
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_FIELD_REPORT

END 

BEGIN -- CREATE IMP 

					select   OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 			 
							,fields here
							,fields here
							,fields here
			 
			 
			 		--	into [safb_migration].DBO.IMP_XXXXXXXXXXXX
			
					from [safb_dq].dbo.XXXXXX as T1
				 
			 

				 
END -- TC1: XXXXXX

BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_XXXXXXXXXXXX
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_XXXXXXXXXXXX GROUP BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_XXXXXXXXXXXX
	 

END 