
--DEDUPLICATE table for conversion

	select distinct * 
	into [tblVolSignIn_duped]
	from [safb_dq].dbo.[tblVolSignIn]


--donor type.

	select [th].[Donor_Type], count(*) c 
	from [safb_dq].dbo.[tblHeader] as [th]
	group by [th].[Donor_Type]

	select [th].[ID_Num], [th].[FirstName], [th].[LastName], [th].[OrgName], [th].[Donor_Type]
	from [safb_dq].dbo.[tblHeader] as [th] where [th].[Donor_Type]='FB'


--address_type

	select h.Address_Type--, at.CODE_DESCRIPTION, COUNT(*) AS RecCnt
	FROM   dbo.tblHeader AS h 
	--left JOIN dbo.Address_Type AS at ON h.Address_Type = at.CODE
	GROUP BY h.Address_Type--, at.CODE_DESCRIPTION
	ORDER BY h.Address_Type

	
	select h.[XA_Code]--, at.CODE_DESCRIPTION, COUNT(*) AS RecCnt
	FROM   dbo.[tblExtraAddress] AS h 
--	left JOIN dbo.[XA_Code] as at ON h.[XA_Code]= at.CODE
	GROUP BY h.[XA_Code]--, at.CODE_DESCRIPTION
	ORDER BY h.[XA_Code]


	select distinct [XA_Misc] from [dbo].[tblExtraAddress]


--club

	select h.[Club] --, c.[CODE_DESCRIPTION] ClubDesc, Count(*) as RecCnt
	from [dbo].[tblHeader] as h
	--left join dbo.club c on h.[Club]=c.[CODE]
	group by h.[Club]-- , c.[CODE_DESCRIPTION] 
	order by h.[Club]-- , c.[CODE_DESCRIPTION] 

	select * from [dbo].[Club]

--relationship

	select [th].[Relationship_Between_Primary_And_Secondary], count(*)  
	from dbo.[tblHeader] as [th]	
	group by [th].[Relationship_Between_Primary_And_Secondary]
	order by [th].[Relationship_Between_Primary_And_Secondary]

--donation type
	select d.[Donation_Type]--, t.[CODE_DESCRIPTION], Count(*) recordCount
	from [dbo].[tblDonation] d 
	--left join [dbo].[DonationPledge_Type] t on d.[Donation_Type]=t.[CODE]
	group by d.[Donation_Type]--, t.[CODE_DESCRIPTION]



--donation stimulus, appeal

	select	d.[Donation_Stimulus],-- s.[CODE_DESCRIPTION] as StimulusDesc, 
 			d.[Donation_Appeal],-- a.[CODE_DESCRIPTION] as AppealDesc,
			Count(*) RecordCount
	into [safb_dq].dbo.chart_donstiapp
	from [safb_dq].dbo.[tblDonation] d
	--left join [safb_dq].dbo.[DonationPledge_Stimulus] s on d.[Donation_Stimulus]=s.[CODE]
 	--left join [safb_dq].dbo.[DonationPledge_Appeal] as a on d.[Donation_Appeal]=a.[CODE]
	group by d.[Donation_Stimulus], d.[Donation_Appeal]
			--	, s.[CODE_DESCRIPTION],	a.[CODE_DESCRIPTION]
	order by d.[Donation_Appeal] ,  d.[Donation_Stimulus]

	update [safb_dq].dbo.chart_donstiapp
	set [chart_donstiapp].[Donation_Stimulus]='NULL' where [chart_donstiapp].[Donation_Stimulus] is null	

	update [safb_dq].dbo.[chart_donstiapp]
	set [chart_donstiapp].[Donation_Appeal]='NULL' where [chart_donstiapp].[Donation_Appeal] is null

	select [cd].[Donation_Stimulus], null desrip, cd.[Donation_Appeal], null as dsc3 , [cd].[RecordCount]
	from [safb_dq].dbo.[chart_donstiapp] as [cd]
	left join [safb_dq].dbo.[CHART_DonationStimulusAppeal] as [cdsa] on cd.[Donation_Stimulus]=[cdsa].[Donation_Stimulus] and cd.[Donation_Appeal]=[cdsa].[Donation_Appeal]
	where [cdsa].[Donation_Stimulus] is null and [cdsa].[Donation_Appeal] is null
	order by cd.[Donation_Stimulus], cd.[Donation_Appeal]

	select * from [safb_dq].dbo.[tblDonation] as [td] where [td].[Donation_Child_ID]='306542' or [td].[Donation_Child_ID]='1171'



-- donation fund and account code
	drop table [safb_dq].dbo.[chart_fundacct]
	
		alter table [safb_dq].dbo.[tblDonation]
		add stgDonationAccount Varchar(10);
		
		update [safb_dq].dbo.[tblDonation] set stgDonationAccount = [tblDonation].[Donation_Account]  
		update [safb_dq].dbo.[tblDonation] set [tblDonation].stgDonationAccount='NULL' where [tblDonation].stgDonationAccount is null

	--new chart
	select	d.[Donation_Fund]--, f.[CODE_DESCRIPTION] as FundDesc
			,d.stgDonationAccount as [Donation_Account]--, a.[CODE_DESCRIPTION] as AcctDesc
			,Count(*) RecordCount
	into [safb_dq].dbo.chart_fundacct
	from [safb_dq].dbo.[tblDonation] d
--	 left join [safb_dq].dbo.[DonationPledge_Fund] as f on d.[Donation_Fund]=f.[CODE]
--	 left join [safb_dq].dbo.[DonationPledge_Account] as a on d.[Donation_Account]=a.[CODE]
	 group by d.[Donation_Fund]--, f.[CODE_DESCRIPTION]
			, d.stgDonationAccount--, a.[CODE_DESCRIPTION] 
	order by d.[Donation_Fund], d.stgDonationAccount
	--1441

	update [safb_dq].dbo.[chart_fundacct] 
	set [chart_fundacct].[Donation_Fund]='NULL' where [chart_fundacct].[Donation_Fund] is null
	
	update [safb_dq].dbo.[chart_fundacct] 
	set [chart_fundacct].[Donation_Fund]='NULL' where [chart_fundacct].[Donation_Fund] is null

	
	select * from [safb_dq].dbo.chart_fundacct as  c
	left join [safb_dq].dbo.[CHART_DonationFundAccount] as [t1] on c.[Donation_Fund]=t1.[Donation_Fund] and cast(c.[Donation_Account] as nvarchar(30))=t1.[Donation_Account]
	where t1.[Donation_Fund] is null or t1.[Donation_Account] is null
	


	--chart solicitor
	SELECT        dbo.Solicitor.CODE, dbo.tblHeader.Solicitor, dbo.Solicitor.CODE_DESCRIPTION, COUNT(dbo.tblHeader.Solicitor) AS RecCnt
	FROM            dbo.tblHeader RIGHT OUTER JOIN
							 dbo.Solicitor ON dbo.tblHeader.Solicitor = dbo.Solicitor.CODE
	GROUP BY dbo.Solicitor.CODE, dbo.tblHeader.Solicitor, dbo.Solicitor.CODE_DESCRIPTION

	--chart solicitor
	SELECT        dbo.tblHeader.Solicitor, dbo.Solicitor.CODE_DESCRIPTION, COUNT(dbo.tblHeader.Solicitor) AS RecCnt
	FROM            dbo.tblHeader 
				left JOIN dbo.Solicitor ON dbo.tblHeader.Solicitor = dbo.Solicitor.CODE
	GROUP BY dbo.tblHeader.Solicitor, dbo.Solicitor.CODE_DESCRIPTION

	
