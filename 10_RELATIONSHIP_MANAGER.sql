USE [safb_dq]
GO

 
 
BEGIN -- DROP IMP

	DROP TABLE [safb_migration].DBO.IMP_RELATIONSHIP_MANAGER

END 

BEGIN -- CREATE IMP 

					select  distinct OwnerId = [safb_migration].[dbo].[fnc_OwnerId]()     
 			 
							,Account__c = xa.id --LOOKUP: Lookup (Account)	
							,Relationship_Manager__c =  xu.id --LOOKUP: Lookup (User)		 
							,Relationship_Level__c = 'Primary'
							,Start_Date__c =t1.[When_Donor_Added]
							,CreatedDate=  t1.[When_Donor_Added] 
							
							
							--reference

							,cu.USerName as zrefUserName
							,t1.[ID_Num] as zrefLegacyId
							,t1.Solicitor	  as zrefSolicitor

			 			into [safb_migration].DBO.IMP_RELATIONSHIP_MANAGER
			
					from [safb_dq].dbo.[tblHeader] as T1
					join (select * from [safb_dq].dbo.[CHART_Users] 
						  where [CHART_Users].[SourceField]='Solicitor' and [CHART_Users].[Convert]='Yes') as cu
							on  t1.[Solicitor]=cu.[FieldValue]
					JOIN [safb_migration].dbo.xtr_account as xa on t1.[Legacy_Id__c]=xa.legacy_Id__c
					JOIN [safb_migration].dbo.[xtr_Users] as [xu] on cu.UserName = xu.[Username]
					ORDER BY t1.[ID_Num], t1.[Solicitor]

				 
		 
END -- TC1: 658;  tc2: 1,038

BEGIN -- AUDIT


	SELECT * FROM [safb_migration].DBO.IMP_RELATIONSHIP_MANAGER
	WHERE Legacy_ID__c IN (SELECT Legacy_ID__c FROM [safb_migration].DBO.IMP_RELATIONSHIP_MANAGER group BY Legacy_ID__c HAVING COUNT(*)>1)
	ORDER BY Legacy_ID__c

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_RELATIONSHIP_MANAGER
	 

END 