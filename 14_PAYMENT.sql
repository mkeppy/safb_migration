USE [safb_dq]
GO

--Record types 
	/* SAMPLE SAMPLE SAMPLE SAMPLE SAMPLE
			SELECT * FROM [safb_migration].dbo.xtr_recordtype order by sobjecttype
	*/
 
 
BEGIN -- DROP IMP
	DROP TABLE [safb_migration].DBO.IMP_PAYMENT
END 

BEGIN -- CREATE IMP 
					select  npe01__Scheduled_Date__c = case when t1.Donation_Date is not null then t1.[Donation_Date]
															 when t1.[Donation_When_Added] is not null then t1.[Donation_When_Added]
															 when t1.[Donation_When_Last_Edited] is not null then t1.[Donation_When_Last_Edited] 
															 else null end
							,npe01__Payment_Amount__c = t1.Donation_Amount	
							,npe01__Payment_Method__c = cdt.[npe01__Payment_Method__c]
							,Credit_Card_Type__c = cdt.[Credit_Card_Type__c]	
							,Legacy_Batch_Number__c = t1.Donation_Batch_Number	
							,Legacy_Receipt_Number__c = t1.Donation_Receipt_Number	
							,npe01__Check_Reference_Number__c = t1.Donation_Check_Number	
							,Check_Date__c =Iif([t1].[Donation_Check_Date] is null,null, Cast([t1].[Donation_Check_Date] as DateTime2)) 
							,CreatedDate = case when t1.Donation_When_Added	is null 
												then t1.[Donation_When_Last_Edited]
												else t1.[Donation_When_Added]  end
							,npsp__Payment_Acknowledged_Date__c = t1.Donation_When_Acknowledged	
							,npe01__Opportunity__c = xt.id
							,npe01__Paid__c = 'TRUE'	-- Set Paid = true			 
			 
							--reference
							,t1.[Donation_Child_ID] as zrefDonationId

			 			into [safb_migration].DBO.IMP_PAYMENT
			
					from [safb_dq].dbo.[tblDonation] as T1
					inner join [safb_migration].dbo.xtr_opportunity as xt on  'DQ-'+Cast(t1.Donation_Child_ID	as Varchar(30))=xt.Legacy_Id__c 
				 	left join [safb_dq].dbo.[CHART_DonationType] as [cdt] on [t1].[Donation_Type] = [cdt].[Donation_Type] 
					order by xt.accountid, xt.id, t1.[Donation_Parent_ID]

END -- TC1: 319434, tc2: 454,778


BEGIN -- AUDIT

	SELECT * FROM [safb_migration].DBO.IMP_PAYMENT
	WHERE zrefDonationId IN (SELECT zrefDonationId FROM [safb_migration].DBO.IMP_PAYMENT group BY zrefDonationId HAVING COUNT(*)>1)
	ORDER BY zrefDonationId

  	SELECT COUNT(*) FROM [safb_migration].DBO.IMP_PAYMENT
	
	SELECT * FROM [safb_migration].DBO.IMP_PAYMENT 
	where npe01__Opportunity__c  is null or npe01__Scheduled_Date__c is null



END 


begin---errors

	select * from [safb_migration].dbo.imp_payment_xerror1

	
	SELECT p.* FROM [safb_migration].DBO.IMP_PAYMENT p
	join [safb_migration].dbo.xtr_opportunity xo on 'DQ-'+cast(p.[zrefDonationId] as nvarchar(20))=xo.legacy_Id__c
	left join [safb_migration].dbo.xtr_payment xp on xo.Id=xp.npe01__Opportunity__c
	where xp.id is null
	 

	
	select [td].[Donation_Check_Date], IsDate( [td].[Donation_Check_Date]) testCheckDate
			,Iif([td].[Donation_Check_Date] is null,null, Cast([td].[Donation_Check_Date] as DateTime2)) test
			, [td].[Donation_When_Added]
	from [safb_dq].dbo.[tblDonation] as [td]
	where [td].[Donation_Check_Date] is not null
	order by [testCheckDate] 


























